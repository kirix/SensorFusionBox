#ifndef _BoxMegaWeMosIf_h
#define _BoxMegaWeMosIf_h

#include <Arduino.h>
#include <SoftDelay.h>
#include <stdio.h>
#include <pb_encode.h>
#include <pb_decode.h>
#include <simple.pb.h>
#include <boxMegaWemos.pb.h>
#include <boxWemosMega.pb.h>


typedef void (*WifiDataReceived)(WifiStatusMsg);
typedef void (*WifiIPAddressReceived)(const char*);
typedef void (*WifiNetworkReceived)(WifiNetworkMsg);
typedef void (*DataFinished)();
//uint8_t sendBuffer[63];
class BoxMegaWeMosIf {

	static MegaWemosMsgs sendMsg;
	static uint8_t sendBuffer[63];

	static WifiDataReceived WiFiStatusCallback;
	static WifiIPAddressReceived WiFiIPAddressCallback;
	static WifiNetworkReceived WifiNetworkCallback;
	static DataFinished DataFinishedCallback;
	static bool WemosReplied;
	SoftDelay ReplyTimeout;

public:

	int DataCallbacksCount = 0;

	BoxMegaWeMosIf() {
	}

	void static requestWifiStatus(WifiDataReceived receiveWifiStatusCallback) {
		WiFiStatusCallback = receiveWifiStatusCallback;

		sendMsg = MegaWemosMsgs_init_zero;
		sendMsg.payload.requestMsg.type = RequestMsg_RequestType_WifiStatus;
		sendMsg.which_payload = MegaWemosMsgs_requestMsg_tag;
		sendMessage(&sendMsg);
	}

	void static requestWifiNetworks(WifiNetworkReceived wifiNetworkCallback, DataFinished dataFinishedCallback) {
		WifiNetworkCallback = wifiNetworkCallback;
		DataFinishedCallback = dataFinishedCallback;

		sendMsg = MegaWemosMsgs_init_zero;
		sendMsg.payload.requestMsg.type = RequestMsg_RequestType_WifiNetworks;
		sendMsg.which_payload = MegaWemosMsgs_requestMsg_tag;
		sendMessage(&sendMsg);
	}

	void static proccesSerialData(uint8_t* buffer, size_t message_length) {
		WemosMegaMsgs message = WemosMegaMsgs_init_zero;

		/* Create a stream that reads from the buffer. */
		pb_istream_t stream = pb_istream_from_buffer(buffer, message_length);

		/* Now we are ready to decode the message. */
		bool status = pb_decode(&stream, WemosMegaMsgs_fields, &message);

		/* Check for errors... */
		if (!status)
		{
			Serial.println(String(F("Decoding failed: ")) + String(PB_GET_ERROR(&stream)));
			sendMsg.which_payload = MegaWemosMsgs_syncMsg_tag;
			sendMsg.payload.syncMsg.type = SyncMsg_SyncMsgType::SyncMsg_SyncMsgType_ERROR;
			sendMessage(&sendMsg);
		}
		else {
			switch(message.which_payload) {
				case WemosMegaMsgs_wifiConfigDataMsg_tag:
					break;
				case WemosMegaMsgs_wifiStatusMsg_tag:
					if(WiFiStatusCallback != NULL) {
						WiFiStatusCallback(message.payload.wifiStatusMsg);
					}
					sendMsg = MegaWemosMsgs_init_zero;
					sendMsg.which_payload = MegaWemosMsgs_syncMsg_tag;
					sendMsg.payload.syncMsg.type = SyncMsg_SyncMsgType::SyncMsg_SyncMsgType_ACK;

					sendMessage(&sendMsg);
					break;
				case WemosMegaMsgs_wifiIpMsg_tag:
					break;
				case WemosMegaMsgs_wifiNetworkMsg_tag:
					if(WifiNetworkCallback != NULL) {
						WifiNetworkCallback(message.payload.wifiNetworkMsg);
					}

					sendMsg = MegaWemosMsgs_init_zero;
					sendMsg.which_payload = MegaWemosMsgs_syncMsg_tag;
					sendMsg.payload.syncMsg.type = SyncMsg_SyncMsgType::SyncMsg_SyncMsgType_ACK;

					sendMessage(&sendMsg);
					break;
				case WemosMegaMsgs_syncMsg_tag:
					switch(message.payload.syncMsg.type) {
						case SyncMsg_SyncMsgType_FINISH:
							WifiNetworkCallback = NULL;
							DataFinishedCallback();
							DataFinishedCallback = NULL;
						break;
						default:
						break;
					}
					break;
			}
		}
	}

	void static sendMessage(MegaWemosMsgs* megaWemosMsgs) {
		pb_ostream_t stream = pb_ostream_from_buffer(sendBuffer, sizeof(sendBuffer));
		bool status = pb_encode(&stream, MegaWemosMsgs_fields, megaWemosMsgs);
		size_t message_length = stream.bytes_written;
		if (!status)
		{
			Serial.println(String(F("Encoding failed: ")) +  String(PB_GET_ERROR(&stream)));
		}
		//Serial.println("sendMessage length =" + String(message_length));
		Serial1.write(message_length);
		Serial1.write(sendBuffer, message_length);
		memset(&sendBuffer[0], 0, sizeof(sendBuffer));
		*megaWemosMsgs = MegaWemosMsgs_init_zero;
	}

	static void update() {
		/*if(!WemosReplied && ReplyTimeout.isTime()) {
			cleanSerial1();
			sendRequest(Request);
		}*/
	}
};


#endif

