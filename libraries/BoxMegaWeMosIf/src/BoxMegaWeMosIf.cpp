///*
// * BoxMegaWeMosIf.cpp
// *
// *  Created on: Nov 8, 2016
// *      Author: kirikaki
// */

#ifndef _BoxMegaWeMosIf_cpp
#define _BoxMegaWeMosIf_cpp

#include <BoxMegaWeMosIf.h>

MegaWemosMsgs BoxMegaWeMosIf::sendMsg = MegaWemosMsgs_init_zero;
uint8_t BoxMegaWeMosIf::sendBuffer[63];

WifiDataReceived BoxMegaWeMosIf::WiFiStatusCallback = NULL;
WifiIPAddressReceived BoxMegaWeMosIf::WiFiIPAddressCallback = NULL;
WifiNetworkReceived BoxMegaWeMosIf::WifiNetworkCallback = NULL;
DataFinished BoxMegaWeMosIf::DataFinishedCallback = NULL;

#endif
