/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.7-dev at Tue Sep 20 22:11:39 2016. */

#ifndef PB_BOXMEGAWEMOS_PB_H_INCLUDED
#define PB_BOXMEGAWEMOS_PB_H_INCLUDED
#include <pb.h>

#include "boxCommon.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Enum definitions */
typedef enum _RequestMsg_RequestType {
    RequestMsg_RequestType_WifiConfigData = 0,
    RequestMsg_RequestType_WifiStatus = 1,
    RequestMsg_RequestType_WifiIPAddress = 2,
    RequestMsg_RequestType_WifiNetworks = 3
} RequestMsg_RequestType;
#define _RequestMsg_RequestType_MIN RequestMsg_RequestType_WifiConfigData
#define _RequestMsg_RequestType_MAX RequestMsg_RequestType_WifiNetworks
#define _RequestMsg_RequestType_ARRAYSIZE ((RequestMsg_RequestType)(RequestMsg_RequestType_WifiNetworks+1))

typedef enum _SensorMsg_SensorType {
    SensorMsg_SensorType_BoxSensorUnknown = 0,
    SensorMsg_SensorType_BoxSensorDHT = 1,
    SensorMsg_SensorType_BoxSensorSoil1 = 2,
    SensorMsg_SensorType_BoxSensorSoil2 = 3
} SensorMsg_SensorType;
#define _SensorMsg_SensorType_MIN SensorMsg_SensorType_BoxSensorUnknown
#define _SensorMsg_SensorType_MAX SensorMsg_SensorType_BoxSensorSoil2
#define _SensorMsg_SensorType_ARRAYSIZE ((SensorMsg_SensorType)(SensorMsg_SensorType_BoxSensorSoil2+1))

/* Struct definitions */
typedef struct _RequestMsg {
    RequestMsg_RequestType type;
/* @@protoc_insertion_point(struct:RequestMsg) */
} RequestMsg;

typedef struct _SensorMsg {
    uint32_t digitalPin;
    uint32_t analogPin;
    SensorMsg_SensorType type;
    float primaryValue;
    float secondaryValue;
    float analogValue;
/* @@protoc_insertion_point(struct:SensorMsg) */
} SensorMsg;

typedef struct _MegaWemosMsgs {
    pb_size_t which_payload;
    union {
        RequestMsg requestMsg;
        SyncMsg syncMsg;
        SensorMsg sensorMsg;
    } payload;
/* @@protoc_insertion_point(struct:MegaWemosMsgs) */
} MegaWemosMsgs;

/* Default values for struct fields */

/* Initializer values for message structs */
#define RequestMsg_init_default                  {(RequestMsg_RequestType)0}
#define SensorMsg_init_default                   {0, 0, (SensorMsg_SensorType)0, 0, 0, 0}
#define MegaWemosMsgs_init_default               {0, {RequestMsg_init_default}}
#define RequestMsg_init_zero                     {(RequestMsg_RequestType)0}
#define SensorMsg_init_zero                      {0, 0, (SensorMsg_SensorType)0, 0, 0, 0}
#define MegaWemosMsgs_init_zero                  {0, {RequestMsg_init_zero}}

/* Field tags (for use in manual encoding/decoding) */
#define RequestMsg_type_tag                      1
#define SensorMsg_digitalPin_tag                 1
#define SensorMsg_analogPin_tag                  2
#define SensorMsg_type_tag                       3
#define SensorMsg_primaryValue_tag               4
#define SensorMsg_secondaryValue_tag             5
#define SensorMsg_analogValue_tag                6
#define MegaWemosMsgs_requestMsg_tag             1
#define MegaWemosMsgs_syncMsg_tag                2
#define MegaWemosMsgs_sensorMsg_tag              3

/* Struct field encoding specification for nanopb */
extern const pb_field_t RequestMsg_fields[2];
extern const pb_field_t SensorMsg_fields[7];
extern const pb_field_t MegaWemosMsgs_fields[4];

/* Maximum encoded size of messages (where known) */
#define RequestMsg_size                          2
#define SensorMsg_size                           29
/* MegaWemosMsgs_size depends on runtime parameters */

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define BOXMEGAWEMOS_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
