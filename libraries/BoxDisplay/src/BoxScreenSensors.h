/*
 * BoxScreenSensors.h
 *
 *  Created on: Jun 12, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENBASESENSORS_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENBASESENSORS_H_

//namespace BoxScreens {

//#include <BoxScreenBase.h>
//#include <BoxScreenSensor.h>
//#include <BoxSensor.h>
#include <BoxScreenBase.h>
#include <BoxScreenSectors/SensorSector.h>
#include <BoxScreenSensor.h>
#include <BoxSensor.h>

class BoxScreenSensors: public BoxScreenBase {
public:

	BoxScreenSensors(BoxScreenType screenType, BoxSensorsClass *boxSensors)
		:BoxScreenBase(screenType) {
		this->boxSensors = boxSensors;
		//Serial.println("Constructing BoxScreenSensors...");
		BoxScreenBase::ScreenView = new BoxScreenViews::BoxBaseView(0, 50, 470, BoxScreenViews::UnlimitedHeight, 80);
		//Serial.println("BoxScreenSensors constructed.");

	}

	void drawScreen() {
		BoxDisplayDefinitions::Tft.fillScreen(BoxDisplayDefinitions::BackgroundColor);
		BoxDisplayDefinitions::Tft.setCursor(150, 15);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans14pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);  //Tft->setTextSize(1);
		BoxDisplayDefinitions::Tft.print(F("Box Sensors"));
		if(BoxScreenBase::ScreenView->Sectors.size() == 0) {
			initSectors();
			BoxScreenBase::ScreenView->CurrentSector = ScreenView->Sectors[0];
		}
		BoxScreenBase::drawScreen();
	}

	void updateScreen() {
		if(ScreenView != NULL) {
			if(!ScreenView->IsOnSectorScreen) {
				if(ScreenView->Sectors.size() > 0) {
					ScreenView->Sectors[currentSensorIndex]->updateSector();
					currentSensorIndex++;
					if(currentSensorIndex == ScreenView->Sectors.size()) {
						currentSensorIndex = 0;
					}
				}
			}
		}
	}

	~BoxScreenSensors() {
		boxSensors = NULL;
	}

private:
	int sensorRectWidth = 160;
	int sensorRectHeight = 66;

	int currentSensorIndex = 0;

	BoxSensorsClass *boxSensors;

	void initSectors() {

		Serial.println(String(F("Initializing Sectors... Count:")) + String(boxSensors->Count));
		int rowIndex = 0;
		int columnIndex = 0;
		int columnsCount = 3;

		for(int sensorIndex = 0; sensorIndex < boxSensors->Count; sensorIndex++) {
			if(columnIndex == columnsCount) {
				columnIndex = 0;
				++rowIndex;
			}
			BoxScreenSectors::BaseSector *sensorSector = new BoxScreenSectors::SensorSector(String(sensorIndex + 1), sensorRectWidth * columnIndex, (sensorRectHeight * rowIndex) + 50,
					sensorRectWidth, sensorRectHeight, boxSensors, sensorIndex);
			BoxScreenBase::ScreenView->addSector(sensorSector);
			columnIndex++;
		}
	}

};

//} /* namespace BoxScreens */


#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENBASESENSORS_H_ */
