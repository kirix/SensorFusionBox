/*
 * BoxScreenBase.h
 *
 *  Created on: Jun 12, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENBASE_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENBASE_H_

#include <BoxDisplayDefinitions.h>
#include <BoxScreenViews/BoxBaseView.h>

class BoxScreenBase {
public:

	BoxScreenViews::BoxBaseView *ScreenView = NULL;
	bool IsOnView = false;

	bool HasPreviousScreen = false;
	bool HasNextScreen = false;

	bool HasValue = false;
	String Value = "";

	BoxScreenBase(BoxScreenType screenType) {
		ScreenType = screenType;
		HasPreviousScreen = false;
		HasNextScreen = false;
	}

	BoxScreenType ScreenType = BoxScreenType::BoxScreenTypeUnknown;

	virtual void drawScreen() {
		displayNavigationArrows();
		if(ScreenView != NULL) {
			ScreenView->drawSectors();
		}
	}

	virtual void updateScreen() = 0;

	virtual bool actionOnScreen(BoxActionType actionType) {
		if(ScreenView == NULL) {
			return false;
		}
		else {
			if(IsOnView) {
				IsOnView = ScreenView->actionOnView(actionType);
				if(!IsOnView) {
					if(ScreenView->WasInSectorScreen) {
						IsOnView = true;
						drawScreen();
						ScreenView->HasFocus = true;
						ScreenView->CurrentSector->IsHighLighted(true);
						return true;
					}

					else {
						displayNavigationArrows();
						return false;
					}
				}
				else {
					return true;
				}
			}
			else {
				if(actionType == BoxActionType::BoxActionRotarySwitch) {
					IsOnView = ScreenView->actionOnView(actionType);
					if(IsOnView == true) {
						displayNavigationArrows();
						return true;
					}
				}
				else if(actionType == BoxActionType::BoxActionBack) {
					return false;
				}
			}
		}
		return false;
	}

	virtual ~BoxScreenBase() {
		if(ScreenView != NULL) {
			delete ScreenView;
			ScreenView = NULL;
		}
	}

private:

	void displayNavigationArrows() {
		if(HasNextScreen) {
			int X1X2 = 480 - 18;
			int Y1 = 12;
			int Y2 = 36;
			int X3 = X1X2 + 12;
			int Y3 = Y1 + 12;
			BoxDisplayDefinitions::Tft.fillTriangle(X1X2, Y1, X1X2, Y2, X3, Y3, BoxDisplayDefinitions::SelectedColor);
			if(IsOnView) {
				BoxDisplayDefinitions::Tft.fillTriangle(X1X2, Y1, X1X2, Y2, X3, Y3, BoxDisplayDefinitions::BackgroundColor);
			}
		}

		if(HasPreviousScreen == true) {
			int X1X2 = 18;
			int Y1 = 12;
			int Y2 = 36;
			int X3 = X1X2 - 12;
			int Y3 = Y1 + 12;
			BoxDisplayDefinitions::Tft.fillTriangle(X1X2, Y1, X1X2, Y2, X3, Y3, BoxDisplayDefinitions::SelectedColor);
			if(IsOnView) {
				BoxDisplayDefinitions::Tft.fillTriangle(X1X2, Y1, X1X2, Y2, X3, Y3, BoxDisplayDefinitions::BackgroundColor);
			}
		}
	}

};

//} /* namespace BoxScreens */

#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENBASE_H_ */
