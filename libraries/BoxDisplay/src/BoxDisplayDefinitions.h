/*
 * BoxDisplayDefinitions.h
 *
 *  Created on: Jul 10, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXDISPLAYDEFINITIONS_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXDISPLAYDEFINITIONS_H_

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_TFTLCD.h> // Hardware-specific library

// Assign human-readable names to some common 16-bit color values:
#define	BLACK   0x0000
#define	BLUE    0x001F
#define	RED     0xF800
#define	GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF


// The control pins for the LCD can be assigned to any digital or
// analog pins...but we'll use the analog pins as this allows us to
// double up the pins with the touch screen (see the TFT paint example).

#define LCD_RD 8 //A0 // LCD Read goes to Analog 0
#define LCD_WR 9 //A1 // LCD Write goes to Analog 1
#define LCD_CD 10 //A2 // Command/Data goes to Analog 2
#define LCD_CS 11 //A3 // Chip Select goes to Analog 3
#define LCD_RESET 12 //A4 // Can alternately just connect to Arduino's reset pin
#define LCD_LIGHT 13

typedef enum {
	BoxScreenTypeUnknown, BoxScreenTypeLogo, BoxScreenTypeSensors, BoxScreenTypeSensor, BoxScreenTypeSettings, BoxScreenTypeWiFiNetworks
} BoxScreenType;

typedef enum {
	BoxActionRightShift, BoxActionLeftShift, BoxActionRotarySwitch, BoxActionRotarySwitchLong,
	BoxActionBack, BoxActionBackLong
} BoxActionType;

class BoxDisplayDefinitions {

public:
	static volatile bool *HasInterrupts;

	static String NetworkStatusAlias[];// = { "", "DISCONNECTED", "CONNECTED", "STORED" };

	static Adafruit_TFTLCD Tft;// = Adafruit_TFTLCD(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);

	static uint16_t BackgroundColor;
	static uint16_t ForegroundColor;
	static uint16_t FontColor;
	static uint16_t SelectedColor;
	static uint16_t UnselectedColor;

	static int getTextWidth(const GFXfont *font, String textToCheck) {
		int textWidth = 0;
		for(unsigned int i = 0; i < textToCheck.length(); i++) {
			textWidth += getCharWidth(font, textToCheck[i]);
		}
		return textWidth + 10;
	}

	static int getCharWidth(const GFXfont *font, char charToCheck) {
		uint8_t   c2    = (uint8_t)(charToCheck) - pgm_read_byte(&font->first);
		GFXglyph *glyph = &(((GFXglyph *)pgm_read_word(&font->glyph))[c2]);
		return pgm_read_byte(&glyph->xAdvance);
	}

	static String getTextFitsInWidth(const GFXfont *font, int desiredWidth, String text) {
		int currentWidth = 0;
		for(unsigned int i = 0; i < text.length() - 1; i++) {
			currentWidth += getCharWidth(font, text[i]);
			if(currentWidth >= desiredWidth) {
				return text.substring(0, i);
			}
		}
		return text;
	}
};

#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXDISPLAYDEFINITIONS_H_ */
