///*
// * BoxScreenLogo.h
// *
// *  Created on: Jun 12, 2016
// *      Author: kirikaki
// */
#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENWIFISETTINGS_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENWIFISETTINGS_H_
//namespace BoxScreens {
#include <BoxScreenBase.h>
#include <BoxScreenSectors/WiFiSector.h>
#include <BoxMegaWeMosIf.h>
#include <BoxScreenFactories/KeyboardFactory.h>
#include <BoxScreenViews/BoxListView.h>

class BoxScreenWiFiNetworks: public BoxScreenBase {
public:

	BoxScreenWiFiNetworks(BoxScreenType screenType)
			:BoxScreenBase(screenType) {
		BoxScreenBase::ScreenView = new BoxScreenViews::BoxListView(listViewTopXPosition, listViewTopYPosition, listViewWidth,
				BoxScreenViews::UnlimitedHeight, listViewItemHeight);
		boxScreenFactory = new BoxScreenFactories::KeyboardFactory(F("Enter Password: "));
	}
	void drawScreen() {
		BoxDisplayDefinitions::Tft.fillScreen(BoxDisplayDefinitions::BackgroundColor);
		BoxDisplayDefinitions::Tft.setCursor(130, 15);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans14pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);  //Tft->setTextSize(1);
		BoxDisplayDefinitions::Tft.print(F("WiFi Networks"));
		requestWifiNetworksDelay.begin(30000);
		if(!BoxScreenBase::ScreenView->WasInSectorScreen) {
			requestWifiNetworks();
		}
		BoxScreenBase::drawScreen();
	}

	void updateScreen() {
		if(ScreenView != NULL) {
			ScreenView->updateView();

			if(!ScreenView->IsOnSectorScreen) {
				if(wifiNetWorkReceived.signal != 0) {
					//Serial.println("WifiScreenNetwork != NULL");
					//Serial.println(WifiScreenNetwork.SSID + " Signal: " + String(WifiScreenNetwork.Signal));
					//SSID = "TEST MEMORY";
					if(ScreenView->Sectors.size() < 15) {
						BoxScreenSectors::BaseSector* wifiNetWorkSector = new BoxScreenSectors::WiFiSector(wifiNetWorkReceived.ssid, 0, 0, 0, 0,
								boxScreenFactory, wifiNetWorkReceived.status);
						ScreenView->addSector(wifiNetWorkSector);
						wifiNetWorkSector->updateValue(String(wifiNetWorkReceived.signal));
						if(ScreenView->Sectors.size() == 1) {
							BoxScreenBase::ScreenView->CurrentSector = ScreenView->Sectors[0];
							BoxScreenBase::ScreenView->HasFocus = true;
							BoxScreenBase::ScreenView->CurrentSector->IsHighLighted(true);
							BoxScreenBase::IsOnView = true;
						}
					}
					wifiNetWorkReceived.ssid[0] = 0;
					wifiNetWorkReceived.signal = 0;
				}
				if(requestWifiNetworksDelay.isTime()) {
					requestWifiNetworks();
				}
			}
			else {
				BoxScreenBase::ScreenView->CurrentSector->updateSector();
			}
		}
	}

	static void finished() {
	}

	~BoxScreenWiFiNetworks() {
		if(boxScreenFactory != NULL) {
			delete boxScreenFactory;
			boxScreenFactory = NULL;
		}
	}

private:
	static uint8_t netWorksReceived;
	static WifiNetworkMsg wifiNetWorkReceived;

	int listViewTopXPosition = 0;
	int listViewTopYPosition = 50;
	int listViewWidth = 470;
	int listViewItemHeight = 50;
	SoftDelay requestWifiNetworksDelay;

	BoxScreenFactories::BaseFactory *boxScreenFactory = NULL;

	void requestWifiNetworks() {
		BoxScreenBase::ScreenView->clearSectors(true);
		netWorksReceived = 0;
		//WifiScreenNetwork = {"", 0, NetworkStatus::NetworkStatus_NONE, ""};
		BoxMegaWeMosIf::requestWifiNetworks(//networkReceived, BoxScreenWiFiSettings::finished);
				[](WifiNetworkMsg wifiNetwork) {
			wifiNetWorkReceived = wifiNetwork;
//			SSID =  wifiNetwork.ssid
//			Signal = wifiNetwork.Signal;
//			Status = wifiNetwork.Status;
//			Pass = wifiNetwork.Pass;
//			Serial.println(SSID + " Signal: " + String(Signal));
			netWorksReceived++;
		}, []() {
//			for(int i=0; i < Sectors.size(); i++) {
//				if(i > (netWorksReceived - 1)) {
//					BaseSettingSector* sector = &Sectors[i];
//					delete sector;
//				}
//			}
		});
	}
};
//} /* namespace BoxScreens */
#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENWIFISETTINGS_H_ */
