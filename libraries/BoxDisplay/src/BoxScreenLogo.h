/*
 * BoxScreenLogo.h
 *
 *  Created on: Jun 12, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENLOGO_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENLOGO_H_

//namespace BoxScreens {

#include <BoxScreenBase.h>

class BoxScreenLogo: public BoxScreenBase {
public:


	BoxScreenLogo(BoxScreenType screenType)
			:BoxScreenBase(screenType) {
		Serial.println(F("Constructing BoxScreenLogo..."));
		Serial.println(F("BoxScreenLogo constructed."));
	}

	void drawScreen() {
		BoxDisplayDefinitions::Tft.fillScreen(BoxDisplayDefinitions::BackgroundColor);
		BoxDisplayDefinitions::Tft.setCursor(100, 140);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans18pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);
		BoxDisplayDefinitions::Tft.print(F("SensorFusionBox"));
	}

	void updateScreen() {

	}

	bool actionOnScreen(BoxActionType actionType) {
		return false;
	}

	~BoxScreenLogo() {
	}
};

//} /* namespace BoxScreens */

#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENBASEHOME_H_ */
