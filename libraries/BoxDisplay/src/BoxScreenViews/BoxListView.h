/*
 * BoxListView.h
 *
 *  Created on: Oct 1, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENVIEWS_BOXLISTVIEW_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENVIEWS_BOXLISTVIEW_H_

#include <BoxScreenViews/BoxBaseView.h>
#include <SoftDelay.h>

namespace BoxScreenViews {

class BoxListView: public BoxBaseView {
public:

	int ItemLeftPadding = 10;
	int ItemTopMargin = 5;

	SoftDelay reorderingTimer;
	int itemsReorderingInterval = 800;

	BoxListView(uint16_t topX, uint16_t topY, uint16_t width, uint16_t height, uint16_t itemHeight)
		: BoxBaseView(topX, topY, width, height, itemHeight) {
	}

	void addSector(BoxScreenSectors::BaseSector* sector, bool isStatusSector = false) {
		if(!isStatusSector) {
			sector->Width = Width - 20;
			sector->Height = ItemHeight;
			sector->Xposition = TopX + ItemLeftPadding;
			sector->Yposition =  TopY + (Sectors.size() * (ItemHeight + ItemTopMargin));
		}
		BoxBaseView::addSector(sector, isStatusSector);
	}

	void drawSectors() {
		if(hasGoneNextScreenItem || hasGonePreviousScreenItem) {
			if(CurrentSector != NULL) {
				CurrentSector->drawSector();
				checkOrderToDraw();
			}
		}
		else {
			BoxBaseView::drawSectors();
		}
	}

	void updateView() {
		if(!*BoxDisplayDefinitions::HasInterrupts && !BoxBaseView::IsOnSectorScreen) {
			checkOrderToDraw();
		}
		else {
			if(reorderingTimer.isTime()) {
				*BoxDisplayDefinitions::HasInterrupts = false;
			}
		}
	}

	void selectPreviousSector() {
		if(!hasGoneNextScreenItem) {
			if(CurrentSector != NULL)
					CurrentSector->IsHighLighted(false);

			CurrentSectorIndex--;
			if(CurrentSectorIndex < 0) {
				CurrentSectorIndex = 0;
				CurrentSector = Sectors[CurrentSectorIndex];
				CurrentSector->IsHighLighted(false);
			}
			else {
				reorderingTimer.begin(itemsReorderingInterval);
				CurrentSector = Sectors[CurrentSectorIndex];
				//Serial.println("SectorBottomPosition = " + String(SectorBottomPosition) + " ViewBottomPosition = " + String(ViewBottomPosition));
				if(CurrentSector->Yposition < this->TopY) {
					Sectors[CurrentSectorIndex + 1]->eraseSector();
					CurrentSector->Yposition = this->TopY;
					CurrentSector->drawSector();
					CurrentSector->IsHighLighted(true);
					hasGonePreviousScreenItem = true;
					if(CurrentSectorIndex == 0) {
						BoxBaseView::highlightUpArrow(false);
					}
					if(BoxBaseView::ItemsOverflow) {
						BoxBaseView::highlightDownArrow(true);
					}
				}
			}
			CurrentSector->IsHighLighted(true);
		}
	}

	void selectNextSector() {
		if(!hasGonePreviousScreenItem) {
			if(CurrentSector != NULL)
				CurrentSector->IsHighLighted(false);

			CurrentSectorIndex++;
			if(CurrentSectorIndex >= Sectors.size()) {
				CurrentSectorIndex = Sectors.size() - 1;
				CurrentSector = Sectors[CurrentSectorIndex];
				CurrentSector->IsHighLighted(false);
				BoxBaseView::highlightDownArrow(false);
			}
			else {
				reorderingTimer.begin(itemsReorderingInterval);
				CurrentSector = Sectors[CurrentSectorIndex];
				int SectorBottomPosition = CurrentSector->Yposition + CurrentSector->Height;
				//Serial.println("SectorBottomPosition = " + String(SectorBottomPosition) + " ViewBottomPosition = " + String(ViewBottomPosition));
				if(SectorBottomPosition > BoxBaseView::LastViewablePosition) {
					BoxBaseView::highlightUpArrow(true);
					Sectors[CurrentSectorIndex - 1]->eraseSector();
					CurrentSector->Yposition = LastViewablePosition - this->ItemHeight;
					CurrentSector->drawSector();
					CurrentSector->IsHighLighted(true);
					hasGoneNextScreenItem = true;
				}
			}
			CurrentSector->IsHighLighted(true);
		}
	}

	~BoxListView() {

	}

private:
	bool hasGonePreviousScreenItem = false;
	bool hasGoneNextScreenItem = false;

	void checkOrderToDraw() {
		if(hasGoneNextScreenItem) {
			int startPosition = CurrentSector->Yposition;
			int eraseCount = 0;
			for(int i = CurrentSectorIndex - 1; i >= 0; i--) {
				if(Sectors[i]->Yposition < CurrentSector->Yposition && Sectors[i]->Yposition >= this->TopY) {
					Sectors[i]->eraseSector();
					eraseCount++;
					Sectors[CurrentSectorIndex - (eraseCount)]->drawSector();
				}
				startPosition -= (this->ItemHeight + this->ItemTopMargin);
				Sectors[i]->Yposition = startPosition;
			}
			hasGoneNextScreenItem = false;
		}
		else if(hasGonePreviousScreenItem) {
			int startPosition = this->TopY;
			int eraseCount = 0;
			for(size_t i = CurrentSectorIndex + 1; i < Sectors.size(); i++) {
				int SectorBottomPosition = Sectors[i]->Yposition + Sectors[i]->Height;
				if(Sectors[i]->Yposition > CurrentSector->Yposition && SectorBottomPosition <= this->LastViewablePosition) {
					Sectors[i]->eraseSector();
					eraseCount++;
					Sectors[CurrentSectorIndex + eraseCount]->drawSector();
				}
				startPosition += (this->ItemHeight + this->ItemTopMargin);
				Sectors[i]->Yposition = startPosition;
			}
			hasGonePreviousScreenItem = false;
		}
	}

};

} /* namespace BoxScreenViews */

#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENVIEWS_BOXLISTVIEW_H_ */
