/*
 * BoxBaseView.h
 *
 *  Created on: Oct 1, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENVIEWS_BOXBASEVIEW_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENVIEWS_BOXBASEVIEW_H_

#include <BoxScreenSectors/BaseSector.h>
#include <vector>

namespace BoxScreenViews {

static int UnlimitedHeight = 0;

class BoxBaseView {
public:

	std::vector<BoxScreenSectors::BaseSector*> Sectors;
	int TopX = 0;
	int TopY = 0;
	int Width = 0;
	int Height = 0;
	int ItemHeight = 0;

	int CurrentSectorIndex = 0;
	BoxScreenSectors::BaseSector* CurrentSector = NULL;

	bool IsOnSectorScreen = false;
	bool HasFocus = false;
	bool WasInSectorScreen = false;

	int LastViewablePosition = 320;
	bool ItemsOverflow = false;

	BoxBaseView(uint16_t topX, uint16_t topY, uint16_t width, uint16_t height, uint16_t itemHeight) {
		TopX = topX;
		TopY = topY;
		Width = width;
		Height = height;
		ItemHeight = itemHeight;

		if(Height != BoxScreenViews::UnlimitedHeight) {
			LastViewablePosition = Height + this->TopY;
		}
	}

	virtual void updateView() {};

	bool actionOnView(BoxActionType actionType) {
		if(Sectors.size() > 0) {
			if(CurrentSector == NULL) {
				CurrentSector = Sectors[0];
				CurrentSectorIndex = 0;
			}
			if(!IsOnSectorScreen) {
				switch(actionType) {
				case BoxActionType::BoxActionRightShift:
						selectNextSector();
						return true;
					break;
				case BoxActionType::BoxActionLeftShift:
						selectPreviousSector();
						return true;
					break;
				case BoxActionType::BoxActionRotarySwitch:
					if(!HasFocus) {
						HasFocus = true;
						CurrentSector->IsHighLighted(true);
						return true;
					}
					else {
						IsOnSectorScreen = CurrentSector->actionOnSector(actionType);
						if(!IsOnSectorScreen) {
							CurrentSector->IsHighLighted(false);
							CurrentSector->IsHighLighted(true);
						}
						return true;
					}
					break;
				case BoxActionType::BoxActionBack:
					if(HasFocus) {
						HasFocus = false;
						CurrentSector->IsHighLighted(false);
					}
					WasInSectorScreen = false;
					return false;
				}
			}
			else {
				if(CurrentSector != NULL) {
					IsOnSectorScreen = CurrentSector->actionOnSector(actionType);
					if(!IsOnSectorScreen) {
						WasInSectorScreen = true;
						return false;
					}
					else
					{
						return true;
					}
				}
			}
		}
		else { if(actionType == BoxActionType::BoxActionBack) return false; }
		return true;
	}

	virtual void addSector(BoxScreenSectors::BaseSector* sector, bool isStatusSector = false) {
		if(sector->Yposition <= LastViewablePosition) {
			sector->drawSector();
		}
		else {
			BoxBaseView::ItemsOverflow = true;
			BoxBaseView::highlightDownArrow(true);
		}

		Sectors.push_back(sector);
	}

	virtual void drawSectors() {
		for(size_t i = 0; i < Sectors.size(); i++) {
			if(Sectors.at(i)->Yposition >= this->TopY) {
				Sectors.at(i)->drawSector();
			}
		}
	}

	void highlightUpArrow(bool draw) {
		int X1 = TopX + Width - 9;
		int Y1Y2 = TopY + 8;
		int X2 = X1 + 16;
		int X3 = X1 + 8;
		int Y3 = TopY;
		if(draw) {
			BoxDisplayDefinitions::Tft.fillTriangle(X1, Y1Y2, X2, Y1Y2, X3, Y3, BoxDisplayDefinitions::SelectedColor);
		}
		else {
			BoxDisplayDefinitions::Tft.fillTriangle(X1, Y1Y2, X2, Y1Y2, X3, Y3, BoxDisplayDefinitions::BackgroundColor);
		}
	}

	void highlightDownArrow(bool draw) {
		int X1 = TopX + Width - 9;
		int Y1Y2 = LastViewablePosition - 10;
		int X2 = X1 + 16;
		int X3 = X1 + 8;
		int Y3 = LastViewablePosition - 2;
		if(draw) {
			BoxDisplayDefinitions::Tft.fillTriangle(X1, Y1Y2, X2, Y1Y2, X3, Y3, BoxDisplayDefinitions::SelectedColor);
		}
		else {
			BoxDisplayDefinitions::Tft.fillTriangle(X1, Y1Y2, X2, Y1Y2, X3, Y3, BoxDisplayDefinitions::BackgroundColor);
		}
	}

	void clearSectors(bool erase) {
		ItemsOverflow = false;
		highlightUpArrow(false);
		highlightDownArrow(false);
		//Serial.println("vector capacity before clear = " + String(Sectors.capacity()));
		CurrentSectorIndex = 0;
		CurrentSector = NULL;
		if(Sectors.size() > 0) {
			for(size_t i = 0; i < Sectors.size(); i++) {
				Serial.println(String(F("delete sector = ")) + Sectors[i]->Title);
				int SectorBottomPosition = Sectors[i]->Yposition + Sectors[i]->Height;
				if(erase && (SectorBottomPosition <= LastViewablePosition && Sectors[i]->Yposition >= this->TopY)) {
					Sectors[i]->eraseSector();
				}
				delete Sectors[i];
				Sectors[i] = NULL;
			}
			Sectors.clear();
			std::vector<BoxScreenSectors::BaseSector*>().swap(Sectors);
		}
		//Serial.println("vector capacity after clear = " + String(Sectors.capacity()));
	}

	virtual void selectPreviousSector() {
		if(CurrentSector != NULL)
				CurrentSector->IsHighLighted(false);

		CurrentSectorIndex--;
		if(CurrentSectorIndex < 0) {
			CurrentSectorIndex = Sectors.size() - 1;
			CurrentSector = Sectors[CurrentSectorIndex];
		}
		else {
			CurrentSector = Sectors[CurrentSectorIndex];
		}
		CurrentSector->IsHighLighted(true);
	}

	virtual void selectNextSector() {
		if(CurrentSector != NULL)
			CurrentSector->IsHighLighted(false);

		CurrentSectorIndex++;
		if(CurrentSectorIndex >= Sectors.size()) {
			CurrentSectorIndex = 0;
			CurrentSector = Sectors[CurrentSectorIndex];
		}
		else {
			CurrentSector = Sectors[CurrentSectorIndex];
		}
		CurrentSector->IsHighLighted(true);
	}

	virtual ~BoxBaseView() {
		clearSectors(false);
	}

private:

};

} /* namespace BoxScreenViews */

#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENVIEWS_BOXBASEVIEW_H_ */
