/*
 * BoxDisplayDefinitions.cpp
 *
 *  Created on: Nov 20, 2016
 *      Author: kirikaki
 */

#include <BoxDisplayDefinitions.h>

volatile bool *BoxDisplayDefinitions::HasInterrupts = NULL;
String BoxDisplayDefinitions::NetworkStatusAlias[] = {
		"", "DISCONNECTED", "CONNECTED", "STORED" };
Adafruit_TFTLCD BoxDisplayDefinitions::Tft = Adafruit_TFTLCD(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);

uint16_t BoxDisplayDefinitions::BackgroundColor = BLACK;
uint16_t BoxDisplayDefinitions::ForegroundColor = RED;
uint16_t BoxDisplayDefinitions::FontColor = WHITE;
uint16_t BoxDisplayDefinitions::SelectedColor = GREEN;
uint16_t BoxDisplayDefinitions::UnselectedColor = BoxDisplayDefinitions::Tft.color565(153, 151, 129);


