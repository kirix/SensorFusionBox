/*
 * BoxScreenSettings.h
 *
 *  Created on: Jul 5, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSETTINGS_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSETTINGS_H_

//namespace BoxScreens {

#include <BoxScreenBase.h>
#include <BoxScreenSectors/BaseSettingSector.h>
#include <BoxScreenViews/BoxListView.h>
#include <BoxMegaWeMosIf.h>
#include <BoxScreenFactories/WiFiNetworksFactory.h>

class BoxScreenSettings: public BoxScreenBase {

	//BoxScreenBuilder screenBuilder;
public:

	BoxScreenSettings(BoxScreenType screenType)
		:BoxScreenBase(screenType) {
		//Serial.println("Constructing BoxScreenSettings...");
		BoxScreenBase::ScreenView = new BoxScreenViews::BoxListView(listViewTopXPosition, listViewTopYPosition, listViewWidth,
										BoxScreenViews::UnlimitedHeight, listViewItemHeight);
		//Serial.println("BoxScreenSettings constructed.");

		requestWiFiStatusDelay.begin(1000);
		requestIPAddressDelay.begin(10000);

		boxScreenFactory = new BoxScreenFactories::WiFiNetworksFactory();
	}

	void drawScreen() {
		BoxDisplayDefinitions::Tft.fillScreen(BoxDisplayDefinitions::BackgroundColor);
		BoxDisplayDefinitions::Tft.setCursor(70, 15);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans14pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);  //Tft->setTextSize(1);
		BoxDisplayDefinitions::Tft.print(F("SensorFusionBox Settings"));
		if(BoxScreenBase::ScreenView->Sectors.size() == 0) {
			BoxScreenSectors::BaseSector* baseSector1 = new BoxScreenSectors::BaseSettingSector(F("WiFi Networks"), 10, 50, 0, 0,
					boxScreenFactory);
			BoxScreenSectors::BaseSector* baseSetror2 = new BoxScreenSectors::BaseSettingSector(F("WiFi Status:"), 10, 105, 0, 0, NULL);
			BoxScreenSectors::BaseSector* baseSetror3 = new BoxScreenSectors::BaseSettingSector(F("WiFi IPAddress:"), 10, 160, 0, 0, NULL);
			BoxScreenSectors::BaseSector* baseSetror4 = new BoxScreenSectors::BaseSettingSector(F("LAN Hostname: BoxWemos.local"), 10, 215, 0, 0, NULL);
			BoxScreenBase::ScreenView->addSector(baseSector1);
			BoxScreenBase::ScreenView->addSector(baseSetror2);
			BoxScreenBase::ScreenView->addSector(baseSetror3);
			BoxScreenBase::ScreenView->addSector(baseSetror4);
			BoxScreenBase::ScreenView->CurrentSectorIndex = WifiSettingsSectorIndex;
			BoxScreenBase::ScreenView->CurrentSector = BoxScreenBase::ScreenView->Sectors[WifiSettingsSectorIndex];
		}
		BoxScreenBase::drawScreen();
		BoxMegaWeMosIf::requestWifiStatus([](WifiStatusMsg wifiNetwork) {
			wifiNetWorkReceived = wifiNetwork;
		});
	}

	void updateScreen() {
		if(BoxScreenBase::ScreenView != NULL) {
			if(BoxScreenBase::ScreenView->Sectors.size() > 0) {
				if(!BoxScreenBase::ScreenView->IsOnSectorScreen) {
					if(wifiNetWorkReceived.status != NetworkStatus::NetworkStatus_NONE) {
						BoxScreenBase::ScreenView->Sectors[WifiStatusSectorIndex]->updateValue(
								BoxDisplayDefinitions::NetworkStatusAlias[wifiNetWorkReceived.status] + "     "
								+ String(wifiNetWorkReceived.signal) + '%');
					}

					if(requestWiFiStatusDelay.isTime()) {
						BoxMegaWeMosIf::requestWifiStatus([](WifiStatusMsg wifiNetwork) {
							wifiNetWorkReceived = wifiNetwork;
						});
					}

//					if(WiFiIPAddress != "") {
//						BoxScreenBase::ScreenView->Sectors[WifiIPAddressSectorIndex]->updateValue(WiFiIPAddress);
//					}

					if(requestIPAddressDelay.isTime()) {
	//					BoxMegaWeMosComm->requestWifiIPAddress([](const char* wiFiIPAddress) {
	//						WiFiIPAddress = String(wiFiIPAddress);
	//					});
					}
				}
				else {
					BoxScreenBase::ScreenView->CurrentSector->updateSector();
				}
			}
		}
	}

	~BoxScreenSettings() {
		if(boxScreenFactory != NULL) {
			delete boxScreenFactory;
			boxScreenFactory = NULL;
		}
	}
private:

	typedef enum {
		WifiSettingsSectorIndex , WifiStatusSectorIndex, WifiIPAddressSectorIndex, LanHostNameSectorIndex
	} BoxSectorsIndexes;

	static WifiStatusMsg wifiNetWorkReceived;

	int listViewTopXPosition = 0;
	int listViewTopYPosition = 50;
	int listViewWidth = 470;
	int listViewItemHeight = 50;

	SoftDelay requestWiFiStatusDelay;
	SoftDelay requestIPAddressDelay;

	BoxScreenFactories::BaseFactory *boxScreenFactory = NULL;

};

//} /* namespace BoxScreens */


#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSETTINGS_H_ */
