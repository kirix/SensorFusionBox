/*
 * BoxScreenWiFiNetworksFactory.h
 *
 *  Created on: Nov 22, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORIES_WIFINETWORKSFACTORY_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORIES_WIFINETWORKSFACTORY_H_

#include <BoxScreenFactories/BaseFactory.h>
#include <BoxScreenWiFiNetworks.h>

namespace BoxScreenFactories {

class WiFiNetworksFactory: public BaseFactory {
public:
	WiFiNetworksFactory() {
	}

	BoxScreenBase* buildScreen() {
		return new BoxScreenWiFiNetworks(BoxScreenType::BoxScreenTypeWiFiNetworks);
	}

};

} /* namespace BoxScreenFactories */

#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORIES_WIFINETWORKSFACTORY_H_ */
