/*
 * BoxScreenSensorFactory.h
 *
 *  Created on: Nov 22, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORIES_SENSORFACTORY_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORIES_SENSORFACTORY_H_

#include <BoxScreenFactories/BaseFactory.h>
#include <BoxScreenSensor.h>

namespace BoxScreenFactories {

class SensorFactory: public BaseFactory {

	BoxSensorsClass *boxSensors = NULL;
	int sensorIndex = 0;


public:
	SensorFactory(BoxSensorsClass *boxSensors, int sensorIndex) : BaseFactory() {
		this->boxSensors = boxSensors;
		this->sensorIndex = sensorIndex;
	}

	BoxScreenBase* buildScreen() {
		return new BoxScreenSensor(BoxScreenType::BoxScreenTypeSensor, boxSensors, sensorIndex);
	}

};

} /* namespace BoxScreenFactories */

#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORIES_SENSORFACTORY_H_ */
