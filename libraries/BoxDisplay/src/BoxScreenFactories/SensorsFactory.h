/*
 * BoxScreenSensorsFactory.h
 *
 *  Created on: Nov 22, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORIES_SENSORSFACTORY_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORIES_SENSORSFACTORY_H_

#include <BoxScreenFactories/BaseFactory.h>
#include <BoxScreenSensors.h>

namespace BoxScreenFactories {

class SensorsFactory: public BaseFactory {

	BoxSensorsClass *boxSensors = NULL;

public:
	SensorsFactory(BoxSensorsClass *boxSensors) : BaseFactory() {
		this->boxSensors = boxSensors;
	}

	BoxScreenBase* buildScreen() {
		return new BoxScreenSensors(BoxScreenType::BoxScreenTypeWiFiNetworks, boxSensors);
	}

};

} /* namespace BoxScreenFactories */

#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORIES_SENSORSFACTORY_H_ */
