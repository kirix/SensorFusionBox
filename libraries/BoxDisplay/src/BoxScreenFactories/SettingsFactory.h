/*
 * BoxScreenSettingsFactory.h
 *
 *  Created on: Nov 22, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORIES_SETTINGSFACTORY_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORIES_SETTINGSFACTORY_H_

#include <BoxScreenFactories/BaseFactory.h>
#include <BoxScreenSettings.h>

namespace BoxScreenFactories {

class SettingsFactory: public BaseFactory {
public:
	SettingsFactory() : BaseFactory() {
	}

	BoxScreenBase* buildScreen() {
		return new BoxScreenSettings(BoxScreenType::BoxScreenTypeSettings);
	}

	~SettingsFactory() {
	}
};

} /* namespace BoxScreenFactories */

#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORIES_SETTINGSFACTORY_H_ */
