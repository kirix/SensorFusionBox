/*
 * BoxScreenKeyboardFactory.h
 *
 *  Created on: Nov 22, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORIES_KEYBOARDFACTORY_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORIES_KEYBOARDFACTORY_H_

#include <BoxScreenFactories/BaseFactory.h>
#include <BoxScreenKeyboard.h>

namespace BoxScreenFactories {

class KeyboardFactory: public BaseFactory {

	String caption = "";

public:
	KeyboardFactory(String caption) : BaseFactory() {
		this->caption = caption;
	}

	BoxScreenBase* buildScreen() {
		return new BoxScreenKeyboard(BoxScreenType::BoxScreenTypeSettings, caption);
	}
};

} /* namespace BoxScreenFactories */

#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORIES_KEYBOARDFACTORY_H_ */
