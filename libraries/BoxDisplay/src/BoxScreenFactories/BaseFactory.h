/*
 * BoxScreenFactory.h
 *
 *  Created on: Nov 14, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORY_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORY_H_

#include <BoxScreenBase.h>

namespace BoxScreenFactories {

class BaseFactory {
public:
	BaseFactory() {}

	virtual BoxScreenBase* buildScreen() = 0;

	virtual ~BaseFactory() {};
};

} /* namespace BoxScreenFactories */

#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENFACTORY_H_ */
