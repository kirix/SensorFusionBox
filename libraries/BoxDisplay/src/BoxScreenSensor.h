/*
 * BoxScreenSensor.h
 *
 *  Created on: Jun 20, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSENSOR_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSENSOR_H_

//namespace BoxScreens {

#include <BoxScreenBase.h>
#include <BoxScreenViews/BoxListView.h>
#include <BoxSensor.h>

class BoxScreenSensor: public BoxScreenBase {
public:

	int SensorIndex;

	BoxScreenSensor(BoxScreenType screenType, BoxSensorsClass *boxSensors, int sensorIndex)
		:BoxScreenBase(screenType) {
		SensorIndex = sensorIndex;
	}

	void drawScreen() {
		BoxDisplayDefinitions::Tft.fillScreen(BoxDisplayDefinitions::BackgroundColor);
		BoxDisplayDefinitions::Tft.setCursor(100, 140);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);  //Tft->setTextSize(1);
		String sensorTitle = String(F("Box Sensor ")) + String(SensorIndex + 1);
		BoxDisplayDefinitions::Tft.print(sensorTitle);
	}

	void updateScreen() {

	}

	bool actionOnScreen(BoxActionType actionType) {
		switch(actionType) {
		case BoxActionType::BoxActionBack:
			return false;
			break;
		case BoxActionType::BoxActionLeftShift:
		case BoxActionType::BoxActionRightShift:
			break;
		case BoxActionType::BoxActionRotarySwitch:
			break;
		}
		return false;
	}

	~BoxScreenSensor() {
		// TODO Auto-generated destructor stub
	}

private:
};

//} /* namespace BoxScreens */


#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSENSOR_H_ */
