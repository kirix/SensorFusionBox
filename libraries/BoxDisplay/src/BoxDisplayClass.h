#ifndef _BoxDisplayClass_h
#define _BoxDisplayClass_h

#include <BoxSensor.h>
#include <BoxScreenBase.h>
#include <BoxScreenLogo.h>
#include <BoxScreenFactories/SettingsFactory.h>
#include <BoxScreenFactories/SensorsFactory.h>

//using namespace BoxDisplay;

class BoxDisplayClass {

public:
	bool BOX_DISPLAY_ON = true;

	BoxDisplayClass(BoxSensorsClass *boxSensors, volatile bool* hasInterrupt) {
		Serial.println(F("Constructing BoxDisplayClass..."));
		BoxSensors = boxSensors;
		BoxDisplayDefinitions::HasInterrupts = hasInterrupt;
		setupTFT();
		Serial.println("BoxDisplayClass constructed.");
	}

	void actionsOnScreen(BoxActionType actionType) {
		if(BOX_DISPLAY_ON) {
			if(!isCursorInScreen) {
				switch(actionType) {

				case BoxActionType::BoxActionRightShift:
					selectNextScreen();
					break;
				case BoxActionType::BoxActionLeftShift:
					selectPreviousScreen();
					break;
				case BoxActionType::BoxActionRotarySwitch:
					isCursorInScreen = CurrentScreen->actionOnScreen(actionType);
					break;
				case BoxActionType::BoxActionBack:
					break;
				}
			}
			else {
				isCursorInScreen = CurrentScreen->actionOnScreen(actionType);
			}
		}
	}

	void updateScreen(int index) {
		if(BOX_DISPLAY_ON) {
			if(CurrentScreen == NULL) {
				drawHomeScreen();
			}
			//if(ScreenType == BoxScreenType::BoxScreenTypeSensors) {
			CurrentScreen->updateScreen();
			//}
		}
	}

	void offDisplay() {
		BOX_DISPLAY_ON = false;
		delete BoxBasicScreensFactories[BasicSettingsScreen];
		delete BoxBasicScreensFactories[BasicSensorsScreen];
		delete CurrentScreen;
		CurrentScreen = NULL;
		BoxDisplayDefinitions::Tft.fillScreen(WHITE);
		for(int i =255 ; i>=0; i--) {

			analogWrite(LCD_LIGHT, i);
			if(i < 100) {
				digitalWrite(LCD_CS, HIGH);
				digitalWrite(LCD_CD, HIGH);
				digitalWrite(LCD_WR , HIGH);
				digitalWrite(LCD_RD , HIGH);
				digitalWrite(LCD_RESET , HIGH);
				for(int i = 22; i < 29; i++) {
					digitalWrite(i , HIGH);
				}
			}
			delay(6);
		}
	}

	void onDisplay() {
		for(int i = 0 ; i <= 255; i++) {
			analogWrite(LCD_LIGHT, i);
			delay(6);
		}
		setupTFT();
		drawHomeScreen();
		BOX_DISPLAY_ON = true;
	}

private:

	bool isCursorInScreen = false;

	BoxSensorsClass *BoxSensors = NULL;

	BoxScreenBase* CurrentScreen = NULL;

	BoxScreenFactories::BaseFactory *BoxBasicScreensFactories[2];

	typedef enum {
		BasicSettingsScreen, BasicSensorsScreen
	} BoxBasicScreen;

	int CurrentScreenIndex = 0;
	int NoOfScreens = 2;

	void setupTFT() {

		pinMode(LCD_LIGHT, OUTPUT);
		analogWrite(LCD_LIGHT, 255);
		BOX_DISPLAY_ON = true;

		BoxDisplayDefinitions::Tft.reset();

		uint16_t identifier = BoxDisplayDefinitions::Tft.readID();

		if(identifier == 0x9481) {
			Serial.println(F("Found ILI9481 LCD loading driver..."));
			BoxDisplayDefinitions::Tft.begin(0x9481);
			BoxDisplayDefinitions::Tft.setRotation(3);
		}
		else {
			Serial.println(F("LCD Id was not ILI9481, aborting..."));
		}

	}

	void selectPreviousScreen() {
		CurrentScreenIndex--;
		if(CurrentScreenIndex < 0) {
			CurrentScreenIndex = 0;
			return;
		}
		delete CurrentScreen;
		CurrentScreen = BoxBasicScreensFactories[CurrentScreenIndex]->buildScreen();
		if(CurrentScreenIndex > 0) {
			CurrentScreen->HasPreviousScreen = true;
		}
		else if(CurrentScreenIndex < NoOfScreens - 1) {
			CurrentScreen->HasNextScreen = true;
		}
		CurrentScreen->drawScreen();
	}

	void selectNextScreen() {
		Serial.println("selectNextScreen");
		CurrentScreenIndex++;
		if(CurrentScreenIndex > 1) {
			CurrentScreenIndex = 1;
			return;
		}
		delete CurrentScreen;
		CurrentScreen = BoxBasicScreensFactories[CurrentScreenIndex]->buildScreen();
		if(CurrentScreenIndex > 0) {
			CurrentScreen->HasPreviousScreen = true;
		}
		else if(CurrentScreenIndex < NoOfScreens - 1) {
			CurrentScreen->HasNextScreen = true;
		}
		CurrentScreen->drawScreen();
	}

	void drawHomeScreen() {
		BoxScreenLogo logoScreen = BoxScreenLogo(BoxScreenType::BoxScreenTypeLogo);
		logoScreen.drawScreen();
		delay(500);
		BoxBasicScreensFactories[BasicSettingsScreen] = new BoxScreenFactories::SettingsFactory();
		BoxBasicScreensFactories[BasicSensorsScreen] = new BoxScreenFactories::SensorsFactory(BoxSensors);
		CurrentScreenIndex = 0;
		CurrentScreen = BoxBasicScreensFactories[0]->buildScreen();
		CurrentScreen->HasNextScreen = true;
		CurrentScreen->drawScreen();
	}
};

//} /* namespace BoxDisplay */

#endif

