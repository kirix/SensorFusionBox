/*
 * BoxScreenSensors.h
 *
 *  Created on: Jun 12, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENKEYBOARD_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENKEYBOARD_H_

#include <BoxScreenBase.h>
#include <avr/pgmspace.h>
#include <SoftDelay.h>

class BoxScreenKeyboard: public BoxScreenBase {
public:

	BoxScreenKeyboard(BoxScreenType screenType, String caption)
		:BoxScreenBase(screenType) {
		this->caption = caption;
		BoxScreenBase::ScreenView = new BoxScreenViews::BoxBaseView(0, 78, 470, BoxScreenViews::UnlimitedHeight, 80);
		cursorTimer.begin(500);
	}

	void drawScreen() {
		BoxDisplayDefinitions::Tft.fillScreen(BoxDisplayDefinitions::BackgroundColor);
		BoxDisplayDefinitions::Tft.setCursor(10, 5);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);
		BoxDisplayDefinitions::Tft.print(caption);
		if(BoxScreenBase::ScreenView->Sectors.size() == 0) {
			initSmallLetters();
			initScreenFocusSetCursor();
		}
	}

	void updateScreen() {
		if(cursorTimer.isTime()) {
			if(cursorOn) {
				offCursor();
			}
			else {
				BoxDisplayDefinitions::Tft.print('|');
				BoxDisplayDefinitions::Tft.setCursor(lastCursorXPosition, lastCursorYPosition);
				cursorOn = true;
			}
		}
	}

	bool actionOnScreen(BoxActionType actionType) {
		if(cursorOn) {
			offCursor();
		}
		if(actionType == BoxActionType::BoxActionBack) {
			if(text.length() > 0) {
				backSpace();
				return true;
			}
			return false;
		}
		else if(actionType == BoxActionType::BoxActionBackLong) {
			return false;
		}
		else if(BoxScreenBase::actionOnScreen(actionType) && actionType == BoxActionType::BoxActionRotarySwitch) {
			if(BoxScreenBase::ScreenView->CurrentSector->Title == "<--") {
				if(text.length() > 0) {
					backSpace();
				}
			}
			else if(BoxScreenBase::ScreenView->CurrentSector->Title == "Aa") {
				BoxScreenBase::ScreenView->clearSectors(true);
				if(keyLock == KeyboardFunction::noLock) {
					initCapitalLetters();
					keyLock = KeyboardFunction::capsLock;
				}
				else if(keyLock == KeyboardFunction::capsLock) {
					initSmallLetters();
					keyLock = KeyboardFunction::noLock;
				}
				initScreenFocusSetCursor();
			}
			else if(BoxScreenBase::ScreenView->CurrentSector->Title == "123") {
				BoxScreenBase::ScreenView->clearSectors(true);
				initNums();
				initScreenFocusSetCursor();
			}
			else if(BoxScreenBase::ScreenView->CurrentSector->Title == "OK") {
				BoxScreenBase::HasValue = true;
				BoxScreenBase::Value = text;
				return false;
			}
			else {
				uint8_t charWidth = BoxDisplayDefinitions::getCharWidth(&FreeSans12pt7b, BoxScreenBase::ScreenView->CurrentSector->Title[0]);
				if(((lastCursorXPosition + charWidth + BoxDisplayDefinitions::getCharWidth(&FreeSans12pt7b, '|'))
						<= BoxDisplayDefinitions::Tft.width()) || lastCursorYPosition < 72) {
					BoxDisplayDefinitions::Tft.print(BoxScreenBase::ScreenView->CurrentSector->Title);
					text += BoxScreenBase::ScreenView->CurrentSector->Title;
					lastCursorXPosition = BoxDisplayDefinitions::Tft.getCursorX();
					lastCursorYPosition = BoxDisplayDefinitions::Tft.getCursorY();
				}
			}
		}
		return true;
	}

	~BoxScreenKeyboard() {
	}

private:
	int sensorRectWidth = 48;
	int sensorRectHeight = 58;

	int currentKeyIndex = 0;

	int offsetX = 0;
	int offsetY = 88;

	bool cursorOn = false;
	SoftDelay cursorTimer;

	String caption = "";

	String text = "";

	int lastCursorXPosition = 10;
	int lastCursorYPosition = 48;

	typedef enum {
		noLock, capsLock, numLock
	} KeyboardFunction;

	KeyboardFunction keyLock = KeyboardFunction::noLock;

	void initScreenFocusSetCursor() {
		BoxScreenBase::ScreenView->CurrentSector = ScreenView->Sectors[0];
		BoxScreenBase::ScreenView->HasFocus = true;
		BoxScreenBase::ScreenView->CurrentSector->IsHighLighted(true);
		BoxScreenBase::IsOnView = true;
		BoxDisplayDefinitions::Tft.setCursor(lastCursorXPosition, lastCursorYPosition);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);
	}

	void offCursor() {
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::BackgroundColor);
		BoxDisplayDefinitions::Tft.print('|');
		BoxDisplayDefinitions::Tft.setCursor(lastCursorXPosition, lastCursorYPosition);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);
		cursorOn = false;
	}

	void backSpace() {
		if(cursorOn) {
			offCursor();
		}
		char charToRemove = text[text.length() - 1];
		uint8_t charWidth = BoxDisplayDefinitions::getCharWidth(&FreeSans12pt7b, charToRemove);
		text.remove(text.length() - 1);
		lastCursorXPosition = BoxDisplayDefinitions::Tft.getCursorX() - charWidth;
		if(lastCursorXPosition < 10) {
			Serial.println(F("lastCursorXPosition < 10"));
			lastCursorXPosition = BoxDisplayDefinitions::getTextWidth(&FreeSans12pt7b, text);
			lastCursorYPosition = BoxDisplayDefinitions::Tft.getCursorY() -
					(uint8_t)pgm_read_byte(&FreeSans12pt7b.yAdvance);
		}
		BoxDisplayDefinitions::Tft.setCursor(lastCursorXPosition, lastCursorYPosition);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::BackgroundColor);
		BoxDisplayDefinitions::Tft.print(charToRemove);
		BoxDisplayDefinitions::Tft.setCursor(lastCursorXPosition, lastCursorYPosition);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);
	}

	void initSmallLetters() {
		int rowIndex = 0;
		int columnIndex = 0;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("q"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("w"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("e"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("r"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("t"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;
		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("y"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("u"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("i"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("o"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("p"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		offsetX = 24;
		columnIndex = 0;
		rowIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("a"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("s"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("d"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("f"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("g"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("h"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("j"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("k"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("l"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		offsetX = 24;
		columnIndex = 0;
		rowIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("Aa"),
				sensorRectWidth * columnIndex , (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("z"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("x"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("c"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("v"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("b"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("n"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("m"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("<--"),
				sensorRectWidth * columnIndex + offsetX + 12, (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;

		offsetX = 0;
		columnIndex = 0;
		rowIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("123"),
				sensorRectWidth * columnIndex , (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F(","),
				60, (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F(" "),
				120, (sensorRectHeight * rowIndex) + offsetY,
				240, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("."),
				360, (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("OK"),
				420, (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;
	}

	void initCapitalLetters() {
		int rowIndex = 0;
		int columnIndex = 0;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("Q"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("W"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("E"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("R"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("T"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;
		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("Y"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("U"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("I"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("O"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("P"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		offsetX = 24;
		columnIndex = 0;
		rowIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("A"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("S"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("D"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("F"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("G"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("H"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("J"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("K"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("L"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		offsetX = 24;
		columnIndex = 0;
		rowIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("Aa"),
				sensorRectWidth * columnIndex , (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("Z"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("X"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("C"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("V"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("B"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("N"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("M"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("<--"),
				sensorRectWidth * columnIndex + offsetX + 12, (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;

		offsetX = 0;
		columnIndex = 0;
		rowIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("123"),
				sensorRectWidth * columnIndex , (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F(","),
				60, (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F(" "),
				120, (sensorRectHeight * rowIndex) + offsetY,
				240, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("."),
				360, (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("OK"),
				420, (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;
	}

	void initNums() {
		int rowIndex = 0;
		int columnIndex = 0;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("1"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("2"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("3"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("4"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("5"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;
		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("6"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("7"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("8"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("9"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("0"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		offsetX = 24;
		columnIndex = 0;
		rowIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("@"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("#"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("&"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("*"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("-"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("+"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("="),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("("),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F(")"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		offsetX = 24;
		columnIndex = 0;
		rowIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("Aa"),
				sensorRectWidth * columnIndex , (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("_"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("$"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("\""),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("\'"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F(":"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("~"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("/"),
				sensorRectWidth * columnIndex + offsetX, (sensorRectHeight * rowIndex) + offsetY,
				sensorRectWidth, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("<--"),
				sensorRectWidth * columnIndex + offsetX + 12, (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;

		offsetX = 0;
		columnIndex = 0;
		rowIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("123"),
				sensorRectWidth * columnIndex , (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("!"),
				60, (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F(" "),
				120, (sensorRectHeight * rowIndex) + offsetY,
				240, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("?"),
				360, (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;

		BoxScreenBase::ScreenView->addSector(new BoxScreenSectors::BaseSector(F("OK"),
				420, (sensorRectHeight * rowIndex) + offsetY,
				60, sensorRectHeight));
		columnIndex++;
	}

};

//} /* namespace BoxScreens */


#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENKEYBOARD_H_ */
