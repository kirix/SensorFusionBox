/*
 * BoxScreenBaseSector.h
 *
 *  Created on: Jul 6, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSECTORS_BASESECTOR_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSECTORS_BASESECTOR_H_

#include <BoxScreenBase.h>

namespace BoxScreenSectors {

//typedef enum {
//	BoxSectorNoAction, BoxSectorHightlighted, BoxSectorSelected
//} BoxSectorAction;

class BaseSector {
public:

	BaseSector() {
		// TODO Auto-generated constructor stub
	}

	BaseSector(String title, int xPosition, int yPosition, int width, int height) {
		Title = title;
		Xposition = xPosition;
		Yposition = yPosition;
		Width = width;
		Height = height;
	}

	String Title = "";
	String Value = "";

	int Xposition = 0;
	int Yposition = 0;
	int Width = 0;
	int Height = 0;

	bool IsErased = false;

	virtual void drawSector() {
		IsErased = false;
		BoxDisplayDefinitions::Tft.drawRoundRect(Xposition, Yposition, Width, Height, 4, BoxDisplayDefinitions::ForegroundColor);
		BoxDisplayDefinitions::Tft.setCursor(Xposition + 5, Yposition + 5);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);  //Tft->setTextSize(1);
		BoxDisplayDefinitions::Tft.print(Title);
	};

	virtual void updateSector() {};

	virtual void updateTitle(String title) {};

	virtual void updateValue(String newValue) {};

	virtual bool actionOnSector(BoxActionType actionType) { return false; }

	virtual void IsHighLighted(bool highlighted) {
		if(!IsErased) {
			if(highlighted) {
				BoxDisplayDefinitions::Tft.drawRoundRect(Xposition, Yposition, Width, Height, 4, BoxDisplayDefinitions::SelectedColor);
			}
			else {
				BoxDisplayDefinitions::Tft.drawRoundRect(Xposition, Yposition, Width, Height, 4, BoxDisplayDefinitions::ForegroundColor);
			}
		}
	}

	virtual void eraseSector() {
		IsErased = true;
		BoxDisplayDefinitions::Tft.setCursor(Xposition + 5, Yposition + 5);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::BackgroundColor);  //Tft->setTextSize(1);
		BoxDisplayDefinitions::Tft.print(Title);
		BoxDisplayDefinitions::Tft.drawRoundRect(Xposition, Yposition, Width, Height, 4, BoxDisplayDefinitions::BackgroundColor);
	};

	virtual ~BaseSector() {
	}
};

} /* namespace BoxScreenSectors */

#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSECTORS_BOXSCREENBASESECTOR_H_ */
