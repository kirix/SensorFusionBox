/*
 * BoxScreenSettingSector.h
 *
 *  Created on: Jul 6, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSECTORS_SENSORSSECTOR_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSECTORS_SENSORSSECTOR_H_

#include <BoxScreenFactories/SensorFactory.h>
#include <BoxScreenSensor.h>
#include <BoxScreenSectors/BaseSectorWithScreen.h>

namespace BoxScreenSectors {

class SensorSector : public BaseSectorWithScreen {

public:

	SensorSector(String title, int xPosition, int yPosition, int width, int height, BoxSensorsClass *boxSensors, int sensorIndex)
			: BaseSectorWithScreen(title, xPosition, yPosition, width, height) {
		BoxSensorClass = boxSensors->BoxSensors[sensorIndex];
		BaseSectorWithScreen::ScreenFactory = new BoxScreenFactories::SensorFactory(boxSensors, sensorIndex);
		BaseSectorWithScreen::TitleXPosition = BaseSector::Xposition + 3;
		BaseSectorWithScreen::TitleYPosition = BaseSector::Yposition + 3;
	}

	void drawSector() {
		BaseSector::IsErased = false;
		BoxDisplayDefinitions::BoxDisplayDefinitions::Tft.drawRoundRect(BaseSector::Xposition, BaseSector::Yposition, BaseSector::Width, BaseSector::Height,
				4, BoxDisplayDefinitions::ForegroundColor);
		BoxDisplayDefinitions::BoxDisplayDefinitions::Tft.setCursor(BaseSectorWithScreen::TitleXPosition, BaseSectorWithScreen::TitleYPosition);
		BoxDisplayDefinitions::BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);
		BoxDisplayDefinitions::BoxDisplayDefinitions::Tft.print(BaseSector::Title);

		BoxDisplayDefinitions::BoxDisplayDefinitions::Tft.fillCircle(BaseSector::Xposition + 150, BaseSector::Yposition + 10, 6,
				BoxDisplayDefinitions::UnselectedColor);
	}

	void updateSector() {
		if(!IsOnScreen) {
			String temp = "T:";
			String humidity = "H:";
			String soil = "S:";
			//if(*HasInterrupt) {return;}
			BoxDisplayDefinitions::BoxDisplayDefinitions::Tft.fillRect(BaseSector::Xposition + 8, BaseSector::Yposition+30, BaseSector::Width-10, BaseSector::Height-32, BoxDisplayDefinitions::BackgroundColor);
			switch(BoxSensorClass->SensorType) {

				case BoxSensorDHT:
					BoxDisplayDefinitions::BoxDisplayDefinitions::Tft.fillCircle(BaseSector::Xposition+150, BaseSector::Yposition+10, 6, BoxDisplayDefinitions::SelectedColor);
					BoxDisplayDefinitions::BoxDisplayDefinitions::Tft.setCursor(BaseSector::Xposition+8, BaseSector::Yposition+32);
					BoxDisplayDefinitions::BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
					BoxDisplayDefinitions::BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);  //BoxDisplayDefinitions::Tft.setTextSize(0);
					temp = "T:";
					temp = temp + String(BoxSensorClass->PrimaryValue, 1) + "C";
					BoxDisplayDefinitions::Tft.print(temp);
					BoxDisplayDefinitions::Tft.setCursor(BaseSector::Xposition+8, BaseSector::Yposition+60);
					BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
					humidity = "H:";
					humidity = humidity + String(BoxSensorClass->SecondaryValue, 1) + "%";
					BoxDisplayDefinitions::Tft.print(humidity);
					break;
				case BoxSensorSoil1:
				case BoxSensorSoil2:
					BoxDisplayDefinitions::Tft.fillCircle(BaseSector::Xposition+150, BaseSector::Yposition+10, 6, BoxDisplayDefinitions::SelectedColor);
					BoxDisplayDefinitions::Tft.setCursor(BaseSector::Xposition+8, BaseSector::Yposition+32);
					BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
					BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);  //BoxDisplayDefinitions::Tft.setTextSize(0);
					soil = "S:";
					soil = soil + String(BoxSensorClass->AnalogValue, 0) + "%";
					BoxDisplayDefinitions::Tft.print(soil);
					break;
				case BoxSensorUnknown:
					BoxDisplayDefinitions::Tft.fillCircle(BaseSector::Xposition+150, BaseSector::Yposition+10, 6, BoxDisplayDefinitions::UnselectedColor);
					break;
			}
		}
		else {
			ActionScreen->updateScreen();
		}
	}

	void updateTitle(String title) {
		BoxDisplayDefinitions::Tft.setCursor(Xposition + 10, Yposition + 15);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::BackgroundColor);  //Tft->setTextSize(1);
		BoxDisplayDefinitions::Tft.print(Title);
		BoxDisplayDefinitions::Tft.setCursor(Xposition + 10, Yposition + 15);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);  //Tft->setTextSize(1);
		BoxDisplayDefinitions::Tft.print(title);
		BaseSector::Title = title;
	}

    void updateValue(String newValue) {

    }

    ~SensorSector() {
    	if(BaseSectorWithScreen::ScreenFactory != NULL) {
			delete BaseSectorWithScreen::ScreenFactory;
			BaseSectorWithScreen::ScreenFactory = NULL;
		}
    }

private:
	BoxSensor *BoxSensorClass;
};

} /* namespace BoxScreenSectors */

#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSECTORS_BOXSCREENSETTINGSECTOR_H_ */
