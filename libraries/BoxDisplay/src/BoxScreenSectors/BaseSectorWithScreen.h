/*
 * BaseSectorWithScreen.h
 *
 *  Created on: Jul 10, 2016
 *      Author: kirikaki
 */
#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSECTORS_BASESECTORWITHSCREEN_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSECTORS_BASESECTORWITHSCREEN_H_

#include <BoxScreenBase.h>
#include <BoxScreenFactories/BaseFactory.h>
#include <BoxScreenSectors/BaseSector.h>

namespace BoxScreenSectors {


class BaseSectorWithScreen: public BaseSector {
public:

	bool IsOnScreen = false;
	BoxScreenType ScreenType = BoxScreenType::BoxScreenTypeUnknown;
	BoxScreenBase *ActionScreen = NULL;
	BoxScreenFactories::BaseFactory *ScreenFactory = NULL;

	int TitleXPosition = 0;
	int TitleYPosition = 0;

	String ScreenValue = "";

	BaseSectorWithScreen() : BaseSector() {

	}

	BaseSectorWithScreen(String title, int xPosition, int yPosition, int width, int height)
		: BaseSector(title, xPosition, yPosition, width, height) {
	}

	virtual void drawSector() {
		BaseSector::drawSector();
	};

	virtual void updateSector() {
		if(ActionScreen != NULL) {
			if(IsOnScreen) {
				ActionScreen->updateScreen();
			}
		}
	}

	virtual void updateTitle(String title) = 0;

	virtual void updateValue(String newValue) = 0;

	virtual bool actionOnSector(BoxActionType actionType) {
		if(ActionScreen != NULL) {
			if(!IsOnScreen) {
				switch(actionType) {
				case BoxActionType::BoxActionBack:
					IsOnScreen = false;
					break;
				case BoxActionType::BoxActionLeftShift:
				case BoxActionType::BoxActionRightShift:
					break;
				case BoxActionType::BoxActionRotarySwitch:
					IsOnScreen = true;
					ActionScreen->drawScreen();
					return true;
				}
			}
			else {
				IsOnScreen = ActionScreen->actionOnScreen(actionType);
				if(!IsOnScreen) {
					if(ActionScreen->HasValue) {
						ScreenValue = ActionScreen->Value;
					}
					delete ActionScreen;
					ActionScreen = NULL;
				}
				return IsOnScreen;
			}
		}
		else {
			if(actionType == BoxActionType::BoxActionRotarySwitch) {
				if(ScreenFactory != NULL) {
					ActionScreen = ScreenFactory->buildScreen();
					if(ActionScreen != NULL) {
						IsOnScreen = true;
						ActionScreen->drawScreen();
						return true;
					}
				}
			}
			return false;
		}
	}

	void IsHighLighted(bool highlighted) {
		BaseSector::IsHighLighted(highlighted);
	}

	virtual void eraseSector() {
	};

	virtual ~BaseSectorWithScreen() {
		if(ActionScreen != NULL) {
			delete ActionScreen;
			ActionScreen = NULL;
		}
	}

private:

};

} /* namespace BoxScreenSectors */
#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSECTORS_BASESECTORWITHSCREEN_H_ */
