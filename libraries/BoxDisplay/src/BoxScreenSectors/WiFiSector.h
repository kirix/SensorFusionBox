/*
 * WiFiSector.h
 *
 *  Created on: Nov 22, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSECTORS_WIFISECTOR_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSECTORS_WIFISECTOR_H_

#include <BoxScreenSectors/BaseSectorWithScreen.h>
#include <BoxMegaWeMosIf.h>

namespace BoxScreenSectors {

class WiFiSector: public BoxScreenSectors::BaseSectorWithScreen {
public:
	WiFiSector(String title, int xPosition, int yPosition, int width, int height,
			BoxScreenFactories::BaseFactory *screenFactory, NetworkStatus networkStatus)
				: BaseSectorWithScreen(title, xPosition, yPosition, width, height) {
		BaseSectorWithScreen::ScreenFactory = screenFactory;

		ValueXPosition = 360;
		WiFiStatus = networkStatus;
	}

	void drawSector() {
		BaseSectorWithScreen::TitleXPosition = BaseSector::Xposition + 10;
		BaseSectorWithScreen::TitleYPosition = BaseSector::Yposition + 15;
		BaseSector::IsErased = false;
		BaseSectorWithScreen::IsOnScreen = false;
		BoxDisplayDefinitions::Tft.drawRoundRect(Xposition, Yposition, Width, Height, 4,
				BoxDisplayDefinitions::ForegroundColor);

		//String textToPrint = BoxDisplayDefinitions::getTextFitsInWidth(&FreeSans12pt7b, TitleMaxWidth, Title);
		BoxDisplayDefinitions::Tft.setCursor(BaseSectorWithScreen::TitleXPosition, BaseSectorWithScreen::TitleYPosition);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);
		BoxDisplayDefinitions::Tft.print(Title.substring(0, 20));

		drawSignalLines(false, Value.toInt());

		if(WiFiStatus == NetworkStatus::NetworkStatus_STORED) {
			BoxDisplayDefinitions::BoxDisplayDefinitions::Tft.fillCircle(BaseSector::Xposition + ValueXPosition + 55,
					BaseSector::Yposition + 24, 6, BoxDisplayDefinitions::UnselectedColor);
		}
		else if(WiFiStatus == NetworkStatus::NetworkStatus_CONNECTED) {
			BoxDisplayDefinitions::BoxDisplayDefinitions::Tft.fillCircle(BaseSector::Xposition + ValueXPosition + 55,
					BaseSector::Yposition + 24, 6, BoxDisplayDefinitions::SelectedColor);
		}
		else if(WiFiStatus == NetworkStatus::NetworkStatus_DISCONNECTED) {
			BoxDisplayDefinitions::BoxDisplayDefinitions::Tft.fillCircle(BaseSector::Xposition + ValueXPosition + 55,
					BaseSector::Yposition + 24, 6, BoxDisplayDefinitions::ForegroundColor);
		}

//		BoxDisplayDefinitions::Tft.setCursor(ValueXPosition, BaseSectorWithScreen::TitleYPosition);
//		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
//		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);
//		BoxDisplayDefinitions::Tft.print(BaseSector::Value);
		//Serial.println(Title + " ValueXPosition: X1: " + String(value1X1) + " Y1: " + String(value1Y1) + " W: " + String(value1Width) + " H: " + String(value1Height));
		Redraw = true;
	}

	void updateTitle(String title) {
		//String textToPrint = BoxDisplayDefinitions::getTextFitsInWidth(&FreeSans12pt7b, TitleMaxWidth, Title);
		BaseSectorWithScreen::TitleXPosition = BaseSector::Xposition + 10;
		BaseSectorWithScreen::TitleYPosition = BaseSector::Yposition + 15;
		BoxDisplayDefinitions::Tft.setCursor(BaseSectorWithScreen::TitleXPosition, BaseSectorWithScreen::TitleYPosition);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::BackgroundColor);
		BoxDisplayDefinitions::Tft.print(Title.substring(0, 20));

		//textToPrint = BoxDisplayDefinitions::getTextFitsInWidth(&FreeSans12pt7b, TitleMaxWidth, title);
		BoxDisplayDefinitions::Tft.setCursor(Xposition + 10, Yposition + 15);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);
		BoxDisplayDefinitions::Tft.print(title.substring(0, 20));
		BaseSector::Title = title;
	}

	void updateValue(String newValue) {
		if(BaseSector::Value != newValue || Redraw) {

			drawSignalLines(true, BaseSector::Value.toInt());
			drawSignalLines(false, newValue.toInt());

//
//			BoxDisplayDefinitions::Tft.setCursor(ValueXPosition, BaseSectorWithScreen::TitleYPosition);
//			BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
//			BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::BackgroundColor);
//			BoxDisplayDefinitions::Tft.print(BaseSector::Value);
//
//			BoxDisplayDefinitions::Tft.setCursor(ValueXPosition, BaseSectorWithScreen::TitleYPosition);
//			BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
//			BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);
//			BoxDisplayDefinitions::Tft.print(newValue);

			BaseSector::Value = newValue;
			Redraw = false;
		}
	}

	bool actionOnSector(BoxActionType actionType) {
		if(!BaseSectorWithScreen::actionOnSector(actionType)) {
			if(BaseSectorWithScreen::ScreenValue != "") {
				Serial.println(BaseSectorWithScreen::ScreenValue);
				BaseSectorWithScreen::ScreenValue = "";
				//TODO Handle this situation here...
			}
			return false;
		}
	}

	void eraseSector() {
		IsErased = true;

//		BoxDisplayDefinitions::Tft.setCursor(ValueXPosition, BaseSectorWithScreen::TitleYPosition);
//		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
//		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::BackgroundColor);
//		BoxDisplayDefinitions::Tft.print(BaseSector::Value);

		drawSignalLines(true, BaseSector::Value.toInt());

		BoxDisplayDefinitions::BoxDisplayDefinitions::Tft.fillCircle(BaseSector::Xposition + ValueXPosition + 55,
				BaseSector::Yposition + 24, 6, BoxDisplayDefinitions::BackgroundColor);

		//String textToPrint = BoxDisplayDefinitions::getTextFitsInWidth(&FreeSans12pt7b, TitleMaxWidth, Title);
		BoxDisplayDefinitions::Tft.setCursor(BaseSectorWithScreen::TitleXPosition,
				BaseSectorWithScreen::TitleYPosition);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::BackgroundColor);
		BoxDisplayDefinitions::Tft.print(Title.substring(0, 20));
		BoxDisplayDefinitions::Tft.drawRoundRect(Xposition, Yposition, Width, Height, 4,
				BoxDisplayDefinitions::BackgroundColor);
	}

	~WiFiSector() {
	}

private:
	bool Redraw = false;
	bool Moved = true;

	int ValueXPosition = 0;
	NetworkStatus WiFiStatus = NetworkStatus::NetworkStatus_NONE;

	void drawSignalLines(bool erase, int value) {
		uint16_t selectionColor = BoxDisplayDefinitions::SelectedColor;
		uint16_t shadowColor = BoxDisplayDefinitions::UnselectedColor;
		if(erase) {
			selectionColor = BoxDisplayDefinitions::BackgroundColor;
			shadowColor = BoxDisplayDefinitions::BackgroundColor;
		}
		if(value > 0 && value < 25) {
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition,
					BaseSector::Yposition + 30, 5, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 1,
					BaseSector::Yposition + 30, 5, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 2,
					BaseSector::Yposition + 30, 5, selectionColor);

			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 6,
					BaseSector::Yposition + 25, 10, shadowColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 6 + 1,
					BaseSector::Yposition + 25, 10, shadowColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 6 + 2,
					BaseSector::Yposition + 25, 10, shadowColor);

			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 12,
					BaseSector::Yposition + 20, 15, shadowColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 12 + 1,
					BaseSector::Yposition + 20, 15, shadowColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 12 + 2,
					BaseSector::Yposition + 20, 15, shadowColor);

			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 18,
					BaseSector::Yposition + 15, 20, shadowColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 18 + 1,
					BaseSector::Yposition + 15, 20, shadowColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 18 + 2,
					BaseSector::Yposition + 15, 20, shadowColor);
		}
		else if(value > 25 && value < 60) {
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition,
					BaseSector::Yposition + 30, 5, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 1,
					BaseSector::Yposition + 30, 5, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 2,
					BaseSector::Yposition + 30, 5, selectionColor);

			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 6,
					BaseSector::Yposition + 25, 10, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 6 + 1,
					BaseSector::Yposition + 25, 10, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 6 + 2,
					BaseSector::Yposition + 25, 10, selectionColor);

			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 12,
					BaseSector::Yposition + 20, 15, shadowColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 12 + 1,
					BaseSector::Yposition + 20, 15, shadowColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 12 + 2,
					BaseSector::Yposition + 20, 15, shadowColor);

			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 18,
					BaseSector::Yposition + 15, 20, shadowColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 18 + 1,
					BaseSector::Yposition + 15, 20, shadowColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 18 + 2,
					BaseSector::Yposition + 15, 20, shadowColor);
		}
		else if(value > 60 && value < 80) {
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition,
					BaseSector::Yposition + 30, 5, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 1,
					BaseSector::Yposition + 30, 5, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 2,
					BaseSector::Yposition + 30, 5, selectionColor);


			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 6,
					BaseSector::Yposition + 25, 10, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 6 + 1,
					BaseSector::Yposition + 25, 10, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 6 + 2,
					BaseSector::Yposition + 25, 10, selectionColor);

			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 12,
					BaseSector::Yposition + 20, 15, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 12 + 1,
					BaseSector::Yposition + 20, 15, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 12 + 2,
					BaseSector::Yposition + 20, 15, selectionColor);

			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 18,
					BaseSector::Yposition + 15, 20, shadowColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 18 + 1,
					BaseSector::Yposition + 15, 20, shadowColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 18 + 2,
					BaseSector::Yposition + 15, 20, shadowColor);
		}
		else {
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition,
					BaseSector::Yposition + 30, 5, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 1,
					BaseSector::Yposition + 30, 5, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 2,
					BaseSector::Yposition + 30, 5, selectionColor);

			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 6,
					BaseSector::Yposition + 25, 10, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 6 + 1,
					BaseSector::Yposition + 25, 10, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 6 + 2,
					BaseSector::Yposition + 25, 10, selectionColor);

			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 12,
					BaseSector::Yposition + 20, 15, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 12 + 1,
					BaseSector::Yposition + 20, 15, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 12 + 2,
					BaseSector::Yposition + 20, 15, selectionColor);

			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 18,
					BaseSector::Yposition + 15, 20, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 18 + 1,
					BaseSector::Yposition + 15, 20, selectionColor);
			BoxDisplayDefinitions::Tft.drawFastVLine(BaseSector::Xposition + ValueXPosition + 18 + 2,
					BaseSector::Yposition + 15, 20, selectionColor);
		}
	}
};

} /* namespace BoxScreenSectors */

#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSECTORS_WIFISECTOR_H_ */
