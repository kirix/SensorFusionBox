/*
 * BoxScreenSettingSector.h
 *
 *  Created on: Jul 6, 2016
 *      Author: kirikaki
 */

#ifndef LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSETTINGSECTOR_H_
#define LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSETTINGSECTOR_H_

//#include <BoxScreenWiFiNetworks.h>
#include <BoxScreenSectors/BaseSectorWithScreen.h>

namespace BoxScreenSectors {

class BaseSettingSector : public BaseSectorWithScreen {

public:

	BaseSettingSector(String title, int xPosition, int yPosition, int width, int height,
			BoxScreenFactories::BaseFactory *screenFactory)
			: BaseSectorWithScreen(title, xPosition, yPosition, width, height) {
		BaseSectorWithScreen::ScreenFactory = screenFactory;
	}

	void drawSector() {
		BaseSectorWithScreen::TitleXPosition = BaseSector::Xposition + 10;
		BaseSectorWithScreen::TitleYPosition = BaseSector::Yposition + 15;
		BaseSector::IsErased = false;
		BaseSectorWithScreen::IsOnScreen = false;
		BoxDisplayDefinitions::Tft.drawRoundRect(Xposition, Yposition, Width, Height, 4, BoxDisplayDefinitions::ForegroundColor);


		int16_t value1X1 = 0;
		int16_t value1Y1 = 0;
		uint16_t value1Width = 0;
		uint16_t value1Height = 0;

		BoxDisplayDefinitions::Tft.setCursor(BaseSectorWithScreen::TitleXPosition, BaseSectorWithScreen::TitleYPosition);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);
		BoxDisplayDefinitions::Tft.print(Title);
		BoxDisplayDefinitions::Tft.getTextBounds((char* )Title.c_str(), int16_t(BaseSectorWithScreen::TitleXPosition), int16_t(BaseSectorWithScreen::TitleYPosition),
				&value1X1, &value1Y1, &value1Width, &value1Height);
		ValueXPosition = value1Width + 50;

		BoxDisplayDefinitions::Tft.setCursor(ValueXPosition, BaseSectorWithScreen::TitleYPosition);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);
		BoxDisplayDefinitions::Tft.print(BaseSector::Value);

		if(BaseSectorWithScreen::ScreenFactory != NULL) {
//			int X0X1 = BaseSector::Xposition + BaseSector::Width - 22;
//			int Y0 = BaseSector::Yposition + 16;
//			int Y1 = BaseSector::Yposition + 32;
//			int X2 = X0X1 + 8;
//			int Y2 = Y0 + 8;
//			BoxDisplayDefinitions::Tft.fillTriangle(X0X1, Y0, X0X1, Y1, X2, Y2, BoxDisplayDefinitions::SelectedColor);

			int X0 = BaseSector::Xposition + BaseSector::Width - 36;
			int Y0Y1 = BaseSector::Yposition + 18;
			int X1 = X0 + 16;
			int X2 = X0 + 8;
			int Y2 = Y0Y1 + 12;
			BoxDisplayDefinitions::Tft.fillTriangle(X0, Y0Y1, X1, Y0Y1, X2, Y2, BoxDisplayDefinitions::SelectedColor);
		}
		//Serial.println(Title + " ValueXPosition: X1: " + String(value1X1) + " Y1: " + String(value1Y1) + " W: " + String(value1Width) + " H: " + String(value1Height));
		Redraw = true;
	}

	void updateTitle(String title) {
		BaseSectorWithScreen::TitleXPosition = BaseSector::Xposition + 10;
		BaseSectorWithScreen::TitleYPosition = BaseSector::Yposition + 15;
		BoxDisplayDefinitions::Tft.setCursor(BaseSectorWithScreen::TitleXPosition, BaseSectorWithScreen::TitleYPosition);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::BackgroundColor);
		BoxDisplayDefinitions::Tft.print(Title);
		BoxDisplayDefinitions::Tft.setCursor(BaseSectorWithScreen::TitleXPosition, BaseSectorWithScreen::TitleYPosition);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);  //Tft->setTextSize(1);
		BoxDisplayDefinitions::Tft.print(title);
		BaseSector::Title = title;
	}

	void updateValue(String newValue) {
		if(BaseSector::Value != newValue || Redraw) {
			BaseSectorWithScreen::TitleXPosition = BaseSector::Xposition + 10;
			BaseSectorWithScreen::TitleYPosition = BaseSector::Yposition + 15;
			//Serial.println("Yposition = " + String(Yposition));
			//Serial.println("TitleYPosition = " + String(BaseSectorWithScreen::TitleYPosition));

			BoxDisplayDefinitions::Tft.setCursor(ValueXPosition, BaseSectorWithScreen::TitleYPosition);
			BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
			BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::BackgroundColor);
			BoxDisplayDefinitions::Tft.print(BaseSector::Value);

			BoxDisplayDefinitions::Tft.setCursor(ValueXPosition, BaseSectorWithScreen::TitleYPosition);
			BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
			BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::FontColor);
			BoxDisplayDefinitions::Tft.print(newValue);

			BaseSector::Value = newValue;
			Redraw = false;
		}
	}

	void eraseSector() {
		IsErased = true;
		BoxDisplayDefinitions::Tft.setCursor(ValueXPosition, BaseSectorWithScreen::TitleYPosition);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::BackgroundColor);
		BoxDisplayDefinitions::Tft.print(BaseSector::Value);

		BoxDisplayDefinitions::Tft.setCursor(BaseSectorWithScreen::TitleXPosition,
				BaseSectorWithScreen::TitleYPosition);
		BoxDisplayDefinitions::Tft.setFont(&FreeSans12pt7b);
		BoxDisplayDefinitions::Tft.setTextColor(BoxDisplayDefinitions::BackgroundColor);
		BoxDisplayDefinitions::Tft.print(Title);
		BoxDisplayDefinitions::Tft.drawRoundRect(Xposition, Yposition, Width, Height, 4,
				BoxDisplayDefinitions::BackgroundColor);
	}

	~BaseSettingSector() {
	}

private:
	bool Redraw = false;
	bool Moved = true;

	int ValueXPosition = 0;
};

}//} /* namespace BoxScreenSectors/SettingsSectors */

#endif /* LIBRARIES_BOXDISPLAY_SRC_BOXSCREENSECTORS_BOXSCREENSETTINGSECTOR_H_ */
