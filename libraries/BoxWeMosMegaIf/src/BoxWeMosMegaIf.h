#ifndef _BoxWeMosMegaIf_h
#define _BoxWeMosMegaIf_h

#include <BoxWeMosCommons.h>
#include <ArduinoJson.h>


class BoxWeMosMegaIf {

public:

	void proccesSerialData(String data) {

		if (data.startsWith("getWifiConfigData")) {
			Logging.println(BoxLogging::DEBUG, "Mega send: getWifiConfigData");
			SDStorage.getWiFiConfigData(&SSID_STA, &WiFiPass_STA);
			sendWifiConfigData(String(SSID_STA), String(WiFiPass_STA));
		}
		else if (data.startsWith("getWiFiStatus")){
			Logging.println(BoxLogging::DEBUG, "Mega send: getWiFiStatus");
			int wifiStatus = WiFi.status();

			if(wifiStatus == WL_CONNECTED) {
				sendWiFiStatus(WiFiStatus[WiFi.status()], dbmToPercentage(WiFi.RSSI()));
			} else {
				sendWiFiStatus(WiFiStatus[WiFi.status()], "");
			}
		}
		else if (data.startsWith("getWifiIPAddress")){
			Logging.println(BoxLogging::DEBUG, "Mega send: getWifiIPAddress");
			int wifiStatus = WiFi.status();
			if(wifiStatus == WL_CONNECTED) {
				sendWiFiAPIP(WiFi.localIP().toString());
			}
			else {
				sendWiFiAPIP("");
			}
		}
		else if (data.startsWith("getWifiNetworks")){
			Logging.println(BoxLogging::DEBUG, "Mega send: getWifiNetworks");
			sendWifiNetworks();
		}
		else if(data.startsWith("ack")) {
			Logging.println(BoxLogging::DEBUG, "Mega send: ack()");
			if(sendCallback != NULL) {
				sendIndex++;
				sendCallback(sendIndex);
				if(sendIndex == (sendCount-1)) {
					Logging.println(BoxLogging::DEBUG, "sending finish");
					SoftWareSerial.println("finished");
					sendCallback = NULL;
				}
			}
		}
		else if(data.startsWith("error")) {
			Logging.println(BoxLogging::DEBUG, "Mega send: error()");
			if(sendCallback != NULL) {
				sendCallback(sendIndex);
			}
		}
		else {
			processSerial(data);
		}
	}

private:

	String dbmToPercentage(int dBm) {
		String percentance = "0%";
		if(dBm < -1 && dBm >= -45) {
			percentance = "100%";
		}
		else if(dBm < -45 && dBm >= -50) {
			percentance = "90%";
		}
		else if(dBm < -50 && dBm >= -60) {
			percentance = "80%";
		}
		else if(dBm < -60 && dBm >= -65) {
			percentance = "70%";
		}
		else if(dBm < -65 && dBm >= -70) {
			percentance = "60%";
		}
		else if(dBm < -70 && dBm >= -75) {
			percentance = "50%";
		}
		else if(dBm < -75 && dBm >= -80) {
			percentance = "40%";
		}
		else if(dBm < -75 && dBm >= -85) {
			percentance = "30%";
		}
		else if(dBm < -85 && dBm >= -95) {
			percentance = "20%";
		}
		else {
			percentance = "0%";
		}
		return percentance;
	}

	void processSerial(String data) {

		StaticJsonBuffer<1600> jsonBuffer;
		JsonObject& json = jsonBuffer.parseObject(data, 15);

		if (json.success()) {\
			Logging.println(BoxLogging::TRACE, "processJsonSerial");
			if(json.containsKey(JsonObjectKey("boxSensors"))) {
				//json.prettyPrintTo(Serial);
			}
			else if(json.containsKey(JsonObjectKey("wifiSTAConfig"))) {
//				setup_wifi(SSID_STA, WiFiPass_STA);
//				setup_NTP();
			}
			else
			{
				Logging.println(BoxLogging::DEBUG, data);
			}

		}
	}

	void sendWifiConfigData(String wifiSSID, String wifiPASS) {
		Logging.println(BoxLogging::TRACE, "sending WifiConfigData to Mega. SSID: " + wifiSSID + " PASS: " + wifiPASS);
		StaticJsonBuffer<500> jsonSensorsBuffer;
		JsonObject& root = jsonSensorsBuffer.createObject();
		JsonObject& jsonWiFiConfig = root.createNestedObject("wifiConfig");
		jsonWiFiConfig["wifiSSID"] = wifiSSID;
		jsonWiFiConfig["wifiPASS"] = wifiPASS;
		String wifiData;
		root.printTo(wifiData);
		root.printTo(SoftWareSerial);
		Logging.println(BoxLogging::TRACE, "WifiConfigDataJson: " + wifiData);
		SoftWareSerial.println();
	}

	void sendWiFiStatus(String status, String signal) {
		Logging.println(BoxLogging::TRACE, "sending WiFiStatus to Mega. status: " + status + " signal: " + signal);
		StaticJsonBuffer<500> jsonSensorsBuffer;
		JsonObject& root = jsonSensorsBuffer.createObject();
		JsonObject& jsonWiFiConfig = root.createNestedObject("wifiStatus");
		jsonWiFiConfig["status"] = status;
		jsonWiFiConfig["signal"] = signal;
		String wifiData;
		root.printTo(wifiData);
		root.printTo(SoftWareSerial);
		SoftWareSerial.println();
		Logging.println(BoxLogging::TRACE, "WiFiStatusJson: " + wifiData);
	}

	void sendWiFiAPIP(String wifiIPAddress) {
		Logging.println(BoxLogging::TRACE, "sending wifiIPAddress to Mega. IPAddress: " + wifiIPAddress);
		StaticJsonBuffer<500> jsonSensorsBuffer;
		JsonObject& root = jsonSensorsBuffer.createObject();
		root["wifiIPAddress"] = wifiIPAddress;
		String wifiData;
		root.printTo(wifiData);
		root.printTo(SoftWareSerial);
		SoftWareSerial.println();
		Logging.println(BoxLogging::TRACE, "wifiIPAddressJson: " + wifiData);
	}

	void sendWifiNetworks() {
		Logging.println(BoxLogging::TRACE, "collecting WifiNetworks...");
		sendIndex = 0;
		WifiNetworks.clear();
		WifiNetwork network = WifiNetwork();
		int numSsid = WiFi.scanNetworks();
		if(numSsid > 0) {
			for (int thisNet = 0; thisNet < numSsid; thisNet++) {
				network.SSID = String(WiFi.SSIDS(thisNet));
				network.Signal = WiFi.RSSIS((uint8_t)thisNet);
				if(network.SSID == SSID_STA) {
					if(WiFi.status() == WL_CONNECTED) {
						network.Status = (WifiStatus)WifiStatusConnected;
					}
					else {
						network.Status = (WifiStatus)WifiStatusDisconnected;
					}
				}
				else {
					network.Status = (WifiStatus)WifiStatusNone;
				}
				network.Pass = "***";
				Logging.println(BoxLogging::TRACE, "NetWork = " + network.SSID);
				WifiNetworks.push_back(network);
				Logging.println(BoxLogging::TRACE, "WifiNetworks size = " + String(WifiNetworks.size()));
			}
			sendCallback = sendNetwork;
			sendCount = numSsid;
			sendIndex = 0;
			sendCallback(sendIndex);
		}
	}

	static void sendNetwork(unsigned int index) {
		if(WifiNetworks.size() > 0) {
			if(index < WifiNetworks.size()) {
				Logging.println(BoxLogging::TRACE, "sending WifiNetwork to Mega.");
				WifiNetwork network = WifiNetworks[index];
				StaticJsonBuffer<300> jsonSensorsBuffer;
				JsonObject& root = jsonSensorsBuffer.createObject();
				JsonObject& jsonNetwork = root.createNestedObject("wifiNetwork");
				jsonNetwork["ssid"] = network.SSID;
				jsonNetwork["signal"] = network.Signal;
				if(network.SSID == SSID_STA) {
					if(WiFi.status() == WL_CONNECTED) {
						jsonNetwork["status"] = (int)WifiStatusConnected;
					}
					else {
						jsonNetwork["status"] = (int)WifiStatusDisconnected;
					}
				}
				else {
					jsonNetwork["status"] = (int)WifiStatusNone;
				}
				jsonNetwork["pass"] = "***";
				String wifiData;
				root.printTo(wifiData);
				Logging.println(BoxLogging::TRACE, "WifiConfigDataJson: " + wifiData);
				root.printTo(SoftWareSerial);
				SoftWareSerial.println();
			}
		}
	}
};


#endif

