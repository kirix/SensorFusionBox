const uint8_t Helvetica_Normal12pt7bBitmaps[] PROGMEM = {
  0xFF, 0xFF, 0xFF, 0xD3, 0xF0, 0xEF, 0xDF, 0xBF, 0x64, 0x48, 0x80, 0x06,
  0x30, 0x33, 0x01, 0x18, 0x18, 0xC0, 0xC6, 0x3F, 0xFD, 0xFF, 0xE1, 0x98,
  0x18, 0xC0, 0xC6, 0x3F, 0xFD, 0xFF, 0xE1, 0x88, 0x18, 0xC0, 0xC6, 0x06,
  0x30, 0x31, 0x80, 0x04, 0x01, 0xF8, 0x7F, 0xC6, 0x4E, 0xC4, 0x6C, 0x46,
  0xC4, 0x0E, 0x40, 0x7C, 0x03, 0xF8, 0x0F, 0xC0, 0x4E, 0x04, 0x7C, 0x47,
  0xC4, 0x7C, 0x46, 0x64, 0xE3, 0xFC, 0x0E, 0x00, 0x40, 0x04, 0x00, 0x00,
  0x06, 0x0F, 0x80, 0x83, 0xF8, 0x30, 0x61, 0x8C, 0x08, 0x31, 0x81, 0x06,
  0x60, 0x31, 0x8C, 0x07, 0xF3, 0x00, 0x3C, 0x60, 0x00, 0x18, 0x60, 0x03,
  0x3F, 0x00, 0xC6, 0x70, 0x11, 0x86, 0x06, 0x30, 0x41, 0x86, 0x18, 0x30,
  0x67, 0x0C, 0x0F, 0xC1, 0x80, 0x20, 0x0F, 0x80, 0x3F, 0x80, 0xE3, 0x01,
  0x86, 0x03, 0x8C, 0x03, 0x38, 0x07, 0xE0, 0x07, 0x80, 0x3F, 0x00, 0xE6,
  0x31, 0x86, 0x67, 0x0F, 0x8C, 0x0F, 0x18, 0x0E, 0x18, 0x3E, 0x3D, 0xFC,
  0x3F, 0x9C, 0x08, 0x04, 0xFD, 0xB5, 0x00, 0x18, 0xCC, 0x66, 0x31, 0x18,
  0xC6, 0x31, 0x8C, 0x63, 0x18, 0xC3, 0x18, 0xC3, 0x18, 0x60, 0x86, 0x18,
  0xC3, 0x18, 0xC3, 0x18, 0xC6, 0x31, 0x8C, 0x63, 0x31, 0x8C, 0xC6, 0x23,
  0x00, 0x10, 0x23, 0x5F, 0xF3, 0x85, 0x99, 0x00, 0x06, 0x00, 0x60, 0x06,
  0x00, 0x60, 0x06, 0x00, 0x60, 0xFF, 0xF0, 0x60, 0x06, 0x00, 0x60, 0x06,
  0x00, 0x60, 0xFF, 0xB5, 0xA0, 0xFF, 0xF0, 0xFF, 0x80, 0x06, 0x08, 0x10,
  0x60, 0x81, 0x06, 0x08, 0x10, 0x60, 0xC1, 0x06, 0x0C, 0x10, 0x60, 0xC0,
  0x1F, 0x07, 0xF1, 0xC7, 0x30, 0x76, 0x07, 0xC0, 0xF0, 0x1E, 0x03, 0xC0,
  0x78, 0x0F, 0x01, 0xE0, 0x3E, 0x06, 0xC1, 0xD8, 0x31, 0xFE, 0x3F, 0x80,
  0x80, 0x04, 0x31, 0xFF, 0xFC, 0x30, 0xC3, 0x0C, 0x30, 0xC3, 0x0C, 0x30,
  0xC3, 0x0C, 0x1F, 0x03, 0xFC, 0x70, 0xE6, 0x06, 0xE0, 0x6C, 0x06, 0x00,
  0x60, 0x0E, 0x01, 0xC0, 0x78, 0x1E, 0x03, 0x80, 0x70, 0x0E, 0x00, 0xC0,
  0x0F, 0xFE, 0xFF, 0xE0, 0x1F, 0x0F, 0xF9, 0xC7, 0x70, 0x7C, 0x0E, 0x01,
  0xC0, 0x30, 0x7E, 0x0F, 0x80, 0x38, 0x01, 0x80, 0x3C, 0x07, 0x80, 0xF8,
  0x3B, 0xDE, 0x7F, 0x81, 0x80, 0x01, 0x80, 0x38, 0x03, 0x80, 0x78, 0x0D,
  0x81, 0xD8, 0x19, 0x83, 0x18, 0x61, 0x86, 0x18, 0xC1, 0x8F, 0xFF, 0xFF,
  0xF0, 0x18, 0x01, 0x80, 0x18, 0x01, 0x80, 0x3F, 0xE3, 0xFE, 0x60, 0x06,
  0x00, 0x60, 0x06, 0x00, 0x7F, 0x87, 0xFC, 0x60, 0xE0, 0x06, 0x00, 0x60,
  0x06, 0x00, 0x6C, 0x06, 0xE0, 0xE7, 0xBC, 0x3F, 0x80, 0xC0, 0x0F, 0x07,
  0xF8, 0xC3, 0xB0, 0x36, 0x07, 0xC0, 0x30, 0x86, 0xFC, 0xFF, 0xDC, 0x1F,
  0x81, 0xE0, 0x3C, 0x06, 0xC0, 0xD8, 0x1B, 0xCE, 0x3F, 0x81, 0xC0, 0xFF,
  0xFF, 0xFF, 0x00, 0x60, 0x0E, 0x00, 0xC0, 0x18, 0x03, 0x80, 0x30, 0x06,
  0x00, 0x60, 0x0E, 0x00, 0xC0, 0x0C, 0x01, 0xC0, 0x18, 0x01, 0x80, 0x38,
  0x00, 0x1F, 0x07, 0xF9, 0xC3, 0x30, 0x7E, 0x0E, 0xC1, 0xDC, 0x71, 0xFC,
  0x3F, 0xCE, 0x3F, 0x81, 0xE0, 0x3C, 0x07, 0x80, 0xF8, 0x3B, 0xFE, 0x3F,
  0x80, 0x80, 0x1F, 0x0F, 0xF1, 0xC7, 0x70, 0x6C, 0x07, 0x80, 0xF0, 0x1E,
  0x03, 0x60, 0xEF, 0xFC, 0x7D, 0x80, 0x30, 0x07, 0xC1, 0xD8, 0x73, 0xDC,
  0x3F, 0x00, 0x80, 0xFF, 0x80, 0x00, 0x03, 0xFE, 0xFC, 0x00, 0x00, 0x03,
  0xF2, 0xF4, 0x00, 0x30, 0x1F, 0x07, 0x83, 0xE0, 0xF8, 0x0C, 0x00, 0xF8,
  0x01, 0xE0, 0x07, 0x80, 0x1F, 0x00, 0x30, 0xFF, 0xFF, 0xFF, 0x00, 0x00,
  0x00, 0xFF, 0xFF, 0xFF, 0xC0, 0x0F, 0x80, 0x3E, 0x00, 0x78, 0x01, 0xF0,
  0x07, 0x01, 0xF0, 0x7C, 0x3E, 0x0F, 0x80, 0xC0, 0x00, 0x0E, 0x1F, 0xE7,
  0x1F, 0x03, 0xC0, 0xF0, 0x30, 0x0C, 0x06, 0x03, 0x81, 0xC0, 0xE0, 0x30,
  0x0C, 0x00, 0x00, 0x00, 0x30, 0x0C, 0x03, 0x00, 0x00, 0x10, 0x00, 0x07,
  0xFC, 0x00, 0x7D, 0xFC, 0x03, 0x80, 0x38, 0x1C, 0x00, 0x70, 0xC0, 0x00,
  0xE7, 0x0F, 0x99, 0x98, 0x7F, 0xE6, 0xC3, 0x87, 0x0F, 0x0C, 0x1C, 0x3C,
  0x70, 0x70, 0xF1, 0xC1, 0x87, 0xC6, 0x06, 0x1B, 0x1C, 0x38, 0xEC, 0x31,
  0xC7, 0x38, 0xFF, 0xF8, 0x71, 0xE7, 0x81, 0xE0, 0x00, 0x03, 0xC0, 0x00,
  0x03, 0xC1, 0xC0, 0x07, 0xFF, 0x00, 0x01, 0xC0, 0x00, 0x01, 0x80, 0x03,
  0xC0, 0x03, 0xC0, 0x03, 0xE0, 0x07, 0xE0, 0x06, 0x60, 0x0E, 0x70, 0x0E,
  0x70, 0x0C, 0x30, 0x1C, 0x38, 0x18, 0x38, 0x1F, 0xF8, 0x3F, 0xFC, 0x38,
  0x0C, 0x30, 0x0E, 0x70, 0x0E, 0x70, 0x06, 0xE0, 0x07, 0xFF, 0x07, 0xFF,
  0x30, 0x39, 0x80, 0xEC, 0x03, 0x60, 0x1B, 0x01, 0xD8, 0x0C, 0xFF, 0xC7,
  0xFF, 0x30, 0x1D, 0x80, 0x7C, 0x01, 0xE0, 0x0F, 0x00, 0xF8, 0x0E, 0xFF,
  0xF7, 0xFE, 0x00, 0x03, 0xC0, 0x1F, 0xE0, 0x79, 0xE1, 0xC0, 0xE7, 0x00,
  0xCC, 0x01, 0xF8, 0x00, 0x70, 0x00, 0xC0, 0x01, 0x80, 0x03, 0x00, 0x07,
  0x00, 0x0E, 0x00, 0x6C, 0x01, 0xDC, 0x03, 0x1C, 0x0E, 0x3F, 0xF8, 0x1F,
  0xE0, 0x04, 0x00, 0xFF, 0x03, 0xFF, 0x8E, 0x1F, 0x38, 0x0E, 0xE0, 0x3B,
  0x80, 0x7E, 0x01, 0xF8, 0x03, 0xE0, 0x0F, 0x80, 0x3E, 0x00, 0xF8, 0x07,
  0xE0, 0x1F, 0x80, 0x6E, 0x03, 0xB8, 0x1C, 0xFF, 0xF3, 0xFF, 0x00, 0xFF,
  0xF7, 0xFF, 0xB8, 0x01, 0xC0, 0x0E, 0x00, 0x70, 0x03, 0x80, 0x1C, 0x00,
  0xFF, 0xF7, 0xFF, 0xB8, 0x01, 0xC0, 0x0E, 0x00, 0x70, 0x03, 0x80, 0x1C,
  0x00, 0xFF, 0xFF, 0xFF, 0xC0, 0xFF, 0xFF, 0xFF, 0xE0, 0x0E, 0x00, 0xE0,
  0x0E, 0x00, 0xE0, 0x0E, 0x00, 0xFF, 0xEF, 0xFE, 0xE0, 0x0E, 0x00, 0xE0,
  0x0E, 0x00, 0xE0, 0x0E, 0x00, 0xE0, 0x0E, 0x00, 0x03, 0xF0, 0x0F, 0xFC,
  0x1E, 0x1E, 0x38, 0x0E, 0x70, 0x07, 0x60, 0x03, 0xE0, 0x00, 0xE0, 0x00,
  0xC0, 0x00, 0xC0, 0x7F, 0xC0, 0x7F, 0xE0, 0x03, 0xE0, 0x03, 0x60, 0x03,
  0x70, 0x07, 0x38, 0x0F, 0x1F, 0xFB, 0x0F, 0xF3, 0xC0, 0x1E, 0x00, 0xF0,
  0x07, 0x80, 0x3C, 0x01, 0xE0, 0x0F, 0x00, 0x78, 0x03, 0xFF, 0xFF, 0xFF,
  0xF0, 0x07, 0x80, 0x3C, 0x01, 0xE0, 0x0F, 0x00, 0x78, 0x03, 0xC0, 0x1E,
  0x00, 0xC0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0xC0, 0x30,
  0x0C, 0x03, 0x00, 0xC0, 0x30, 0x0C, 0x03, 0x00, 0xC0, 0x30, 0x0C, 0x03,
  0xE0, 0xF8, 0x3E, 0x0D, 0x87, 0x7F, 0x8F, 0xC0, 0xC0, 0x1F, 0x00, 0xEC,
  0x07, 0x30, 0x38, 0xC1, 0xC3, 0x0E, 0x0C, 0x70, 0x33, 0x80, 0xDE, 0x03,
  0xFC, 0x0F, 0x30, 0x38, 0xE0, 0xC1, 0xC3, 0x03, 0x0C, 0x0E, 0x30, 0x1C,
  0xC0, 0x33, 0x00, 0xF0, 0xC0, 0x18, 0x03, 0x00, 0x60, 0x0C, 0x01, 0x80,
  0x30, 0x06, 0x00, 0xC0, 0x18, 0x03, 0x00, 0x60, 0x0C, 0x01, 0x80, 0x30,
  0x06, 0x00, 0xFF, 0xFF, 0xFC, 0xE0, 0x07, 0xF8, 0x03, 0xFC, 0x03, 0xFE,
  0x01, 0xFF, 0x80, 0xFE, 0xC0, 0xFF, 0x60, 0x6F, 0xB8, 0x37, 0xCC, 0x3B,
  0xE6, 0x19, 0xF1, 0x8C, 0xF8, 0xCE, 0x7C, 0x66, 0x3E, 0x1B, 0x1F, 0x0F,
  0x8F, 0x87, 0x87, 0xC1, 0xC3, 0xE0, 0xE1, 0xC0, 0xE0, 0x1F, 0x80, 0x7F,
  0x01, 0xFE, 0x07, 0xF8, 0x1F, 0x70, 0x7C, 0xC1, 0xF3, 0x87, 0xC7, 0x1F,
  0x0C, 0x7C, 0x39, 0xF0, 0x77, 0xC0, 0xDF, 0x03, 0xFC, 0x07, 0xF0, 0x1F,
  0xC0, 0x3F, 0x00, 0x70, 0x03, 0xE0, 0x07, 0xFC, 0x07, 0x9F, 0x07, 0x01,
  0xC7, 0x00, 0x73, 0x00, 0x1B, 0x80, 0x0F, 0x80, 0x07, 0xC0, 0x03, 0xE0,
  0x01, 0xF0, 0x00, 0xF8, 0x00, 0x7E, 0x00, 0x33, 0x00, 0x39, 0xC0, 0x1C,
  0x70, 0x1C, 0x1F, 0x7C, 0x07, 0xFC, 0x00, 0x40, 0x00, 0xFF, 0x87, 0xFF,
  0x3F, 0xFD, 0x80, 0x6C, 0x03, 0xE0, 0x1F, 0x00, 0xF8, 0x06, 0xC0, 0xF7,
  0xFF, 0x30, 0x01, 0x80, 0x0C, 0x00, 0x60, 0x03, 0x00, 0x18, 0x00, 0xC0,
  0x06, 0x00, 0x00, 0x03, 0xE0, 0x07, 0xFC, 0x0F, 0x8F, 0x07, 0x01, 0xC7,
  0x00, 0x77, 0x00, 0x1B, 0x80, 0x0F, 0x80, 0x07, 0xC0, 0x01, 0xE0, 0x00,
  0xF0, 0x00, 0x78, 0x00, 0x7E, 0x00, 0x33, 0x01, 0x19, 0xC1, 0xD8, 0x70,
  0x3C, 0x1F, 0xFE, 0x07, 0xFB, 0x80, 0x40, 0xC0, 0xFF, 0x83, 0xFF, 0xCE,
  0x0F, 0xB8, 0x06, 0xE0, 0x1F, 0x80, 0x7E, 0x01, 0xF8, 0x06, 0xFF, 0xFB,
  0xFF, 0xCE, 0x07, 0xB8, 0x06, 0xE0, 0x1B, 0x80, 0x7E, 0x01, 0xB8, 0x07,
  0xE0, 0x1F, 0x80, 0x70, 0x07, 0x80, 0xFF, 0xC3, 0x87, 0x9C, 0x0E, 0x60,
  0x19, 0x80, 0x67, 0x00, 0x1F, 0x00, 0x3F, 0xC0, 0x3F, 0xC0, 0x0F, 0x80,
  0x07, 0xC0, 0x1F, 0x80, 0x7E, 0x01, 0xDC, 0x07, 0x3E, 0xF8, 0x7F, 0x80,
  0x30, 0x00, 0xFF, 0xFF, 0xFF, 0xF0, 0x38, 0x00, 0xE0, 0x03, 0x80, 0x0E,
  0x00, 0x38, 0x00, 0xE0, 0x03, 0x80, 0x0E, 0x00, 0x38, 0x00, 0xE0, 0x03,
  0x80, 0x0E, 0x00, 0x38, 0x00, 0xE0, 0x03, 0x80, 0x0E, 0x00, 0xC0, 0x1F,
  0x00, 0x7C, 0x01, 0xF0, 0x07, 0xC0, 0x1F, 0x00, 0x7C, 0x01, 0xF0, 0x07,
  0xC0, 0x1F, 0x00, 0x7C, 0x01, 0xF0, 0x07, 0xC0, 0x1F, 0x00, 0x6E, 0x01,
  0x98, 0x0E, 0x78, 0xF0, 0x7F, 0x80, 0x20, 0x00, 0xC0, 0x0F, 0x80, 0x1B,
  0x80, 0x73, 0x00, 0xC6, 0x01, 0x8E, 0x07, 0x0C, 0x0C, 0x18, 0x18, 0x38,
  0x70, 0x30, 0xC0, 0x71, 0x80, 0x67, 0x00, 0xCC, 0x01, 0xD8, 0x01, 0xF0,
  0x03, 0xC0, 0x07, 0x80, 0x06, 0x00, 0xC0, 0x70, 0x1E, 0x03, 0x80, 0xF8,
  0x1C, 0x0F, 0xC1, 0xF0, 0x76, 0x0D, 0x83, 0x30, 0x6C, 0x19, 0xC3, 0x71,
  0xCE, 0x3B, 0x8E, 0x31, 0x8C, 0x61, 0x8C, 0x63, 0x0E, 0xE3, 0xB8, 0x76,
  0x0D, 0xC1, 0xB0, 0x6C, 0x0D, 0x83, 0x60, 0x7C, 0x1F, 0x03, 0xC0, 0x78,
  0x0E, 0x03, 0x80, 0x70, 0x1C, 0x00, 0xE0, 0x0E, 0xE0, 0x39, 0xC0, 0xE1,
  0xC1, 0x81, 0xC7, 0x03, 0x9C, 0x03, 0xB0, 0x03, 0xE0, 0x07, 0x80, 0x07,
  0x00, 0x1F, 0x00, 0x76, 0x01, 0xCE, 0x03, 0x8E, 0x0E, 0x0C, 0x38, 0x1C,
  0x70, 0x1D, 0xC0, 0x1C, 0xE0, 0x07, 0x70, 0x0E, 0x70, 0x0E, 0x38, 0x1C,
  0x18, 0x18, 0x1C, 0x38, 0x0E, 0x30, 0x06, 0x70, 0x07, 0xE0, 0x03, 0xC0,
  0x01, 0xC0, 0x01, 0x80, 0x01, 0x80, 0x01, 0x80, 0x01, 0x80, 0x01, 0x80,
  0x01, 0x80, 0x01, 0x80, 0xFF, 0xFF, 0xFF, 0xC0, 0x0E, 0x00, 0xE0, 0x0E,
  0x00, 0x70, 0x07, 0x00, 0x70, 0x07, 0x00, 0x38, 0x03, 0x80, 0x38, 0x03,
  0x80, 0x1C, 0x01, 0xC0, 0x1C, 0x00, 0xFF, 0xFF, 0xFF, 0xC0, 0xFF, 0xCC,
  0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCF, 0xF0, 0x81, 0x83,
  0x02, 0x06, 0x0C, 0x08, 0x18, 0x30, 0x20, 0x40, 0xC0, 0x81, 0x03, 0x02,
  0x04, 0x0C, 0x18, 0xFF, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33,
  0x33, 0x3F, 0xF0, 0x0C, 0x07, 0x01, 0xE0, 0xD8, 0x33, 0x08, 0xC6, 0x19,
  0x86, 0xC1, 0xC0, 0xFF, 0xFE, 0xE3, 0x0C, 0x30, 0x1F, 0x03, 0xFC, 0x60,
  0xC6, 0x0E, 0x00, 0xE0, 0x1E, 0x3F, 0xE7, 0x8E, 0xE0, 0xEC, 0x0E, 0xC1,
  0xEF, 0x7E, 0x7E, 0x70, 0xC0, 0x0C, 0x00, 0xC0, 0x0C, 0x00, 0xC0, 0x0C,
  0xF8, 0xDF, 0xCF, 0x8E, 0xE0, 0x6E, 0x06, 0xE0, 0x7C, 0x07, 0xC0, 0x7E,
  0x07, 0xE0, 0x6F, 0x0E, 0xFB, 0xCD, 0xF8, 0x06, 0x00, 0x1F, 0x07, 0xF1,
  0xC7, 0x70, 0x6C, 0x0D, 0x80, 0x70, 0x06, 0x00, 0xC0, 0x18, 0x1F, 0x83,
  0x39, 0xE3, 0xF8, 0x18, 0x00, 0x00, 0x30, 0x03, 0x00, 0x30, 0x03, 0x00,
  0x30, 0xF3, 0x3F, 0xF3, 0x8F, 0x70, 0x76, 0x03, 0x60, 0x36, 0x03, 0x60,
  0x36, 0x03, 0x70, 0x77, 0x07, 0x3D, 0xF1, 0xFB, 0x06, 0x00, 0x1F, 0x07,
  0xF9, 0xC3, 0x30, 0x3E, 0x07, 0x80, 0xFF, 0xFF, 0xFF, 0xC0, 0x1C, 0x0D,
  0x83, 0xBD, 0xE1, 0xF8, 0x1C, 0xF3, 0x0C, 0x33, 0xFF, 0xCC, 0x30, 0xC3,
  0x0C, 0x30, 0xC3, 0x0C, 0x30, 0xC0, 0x1E, 0x6F, 0xED, 0xEF, 0xF0, 0x7C,
  0x0F, 0x80, 0xF0, 0x1E, 0x03, 0xC0, 0x78, 0x1F, 0x83, 0xB9, 0xF3, 0xF6,
  0x18, 0xE0, 0x3F, 0x06, 0x7F, 0xC7, 0xE0, 0xC0, 0x30, 0x0C, 0x03, 0x00,
  0xC0, 0x37, 0xCF, 0xFB, 0x87, 0xC0, 0xF0, 0x3C, 0x0F, 0x03, 0xC0, 0xF0,
  0x3C, 0x0F, 0x03, 0xC0, 0xF0, 0x30, 0xFC, 0x3F, 0xFF, 0xFF, 0xF0, 0x18,
  0xC6, 0x00, 0x0C, 0x63, 0x18, 0xC6, 0x31, 0x8C, 0x63, 0x18, 0xC6, 0x31,
  0xFF, 0xC0, 0xC0, 0x18, 0x03, 0x00, 0x60, 0x0C, 0x01, 0x81, 0xF0, 0x66,
  0x18, 0xC6, 0x19, 0x83, 0x70, 0x7F, 0x0E, 0x71, 0x86, 0x30, 0xE6, 0x0E,
  0xC0, 0xD8, 0x1C, 0xFF, 0xFF, 0xFF, 0xFF, 0xF0, 0xDE, 0x3E, 0xFF, 0xFF,
  0xE3, 0xC3, 0xC1, 0x83, 0xC1, 0x83, 0xC1, 0x83, 0xC1, 0x83, 0xC1, 0x83,
  0xC1, 0x83, 0xC1, 0x83, 0xC1, 0x83, 0xC1, 0x83, 0xC1, 0x83, 0xCF, 0x3F,
  0xEF, 0x1F, 0x03, 0xC0, 0xF0, 0x3C, 0x0F, 0x03, 0xC0, 0xF0, 0x3C, 0x0F,
  0x03, 0xC0, 0xC0, 0x0F, 0x07, 0xF1, 0xC7, 0x30, 0x7C, 0x07, 0x80, 0xF0,
  0x1E, 0x03, 0xC0, 0x7C, 0x0F, 0x83, 0xBD, 0xE3, 0xF8, 0xCF, 0x0F, 0xFC,
  0xF1, 0xCE, 0x0E, 0xE0, 0x6C, 0x07, 0xC0, 0x7C, 0x07, 0xE0, 0x7E, 0x06,
  0xF0, 0xEF, 0xFC, 0xDF, 0x8C, 0x40, 0xC0, 0x0C, 0x00, 0xC0, 0x0C, 0x00,
  0x1E, 0x6F, 0xFD, 0xC7, 0xF0, 0x7C, 0x0F, 0x80, 0xF0, 0x1E, 0x03, 0xC0,
  0x78, 0x1F, 0x87, 0xBD, 0xF3, 0xF6, 0x10, 0xC0, 0x18, 0x03, 0x00, 0x60,
  0x0C, 0xDF, 0xFF, 0x30, 0xC3, 0x0C, 0x30, 0xC3, 0x0C, 0x30, 0xC0, 0x1E,
  0x1F, 0xEE, 0x1B, 0x07, 0xC0, 0x3C, 0x07, 0xE0, 0x3E, 0x01, 0xF0, 0x3C,
  0x0F, 0x87, 0x7F, 0x87, 0x00, 0x30, 0xC3, 0x3F, 0xFC, 0xC3, 0x0C, 0x30,
  0xC3, 0x0C, 0x30, 0xC3, 0xCF, 0xC0, 0xF0, 0x3C, 0x0F, 0x03, 0xC0, 0xF0,
  0x3C, 0x0F, 0x03, 0xC0, 0xF0, 0x7C, 0x1F, 0x9F, 0x7E, 0xC4, 0x00, 0xE0,
  0x76, 0x06, 0x60, 0x66, 0x0E, 0x30, 0xC3, 0x0C, 0x31, 0x81, 0x98, 0x1B,
  0x81, 0xB0, 0x0F, 0x00, 0xE0, 0x0E, 0x00, 0xE1, 0xC3, 0xF0, 0xE1, 0xD8,
  0x70, 0xCC, 0x7C, 0x67, 0x36, 0x73, 0x9B, 0x30, 0xCD, 0x98, 0x6C, 0xEC,
  0x3E, 0x3E, 0x0F, 0x1E, 0x07, 0x8F, 0x03, 0x83, 0x81, 0xC1, 0x80, 0x60,
  0x63, 0x0E, 0x38, 0xC1, 0x98, 0x1F, 0x00, 0xF0, 0x06, 0x00, 0xF0, 0x1F,
  0x01, 0x98, 0x31, 0xC7, 0x0C, 0xE0, 0x70, 0xE0, 0x76, 0x06, 0x60, 0x67,
  0x0E, 0x30, 0xC3, 0x0C, 0x39, 0x81, 0x98, 0x1B, 0x81, 0xF0, 0x0F, 0x00,
  0xF0, 0x0E, 0x00, 0xE0, 0x0C, 0x00, 0xC0, 0x7C, 0x07, 0x80, 0xFF, 0xFF,
  0xF0, 0x18, 0x0E, 0x07, 0x03, 0x80, 0xC0, 0x60, 0x38, 0x1C, 0x0E, 0x03,
  0xFF, 0xFF, 0xC0, 0x00, 0xF3, 0x0C, 0x30, 0xC3, 0x0C, 0x30, 0xC6, 0x38,
  0xE1, 0x83, 0x0C, 0x30, 0xC3, 0x0C, 0x30, 0xC1, 0xC0, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xF0, 0xE3, 0x18, 0xC6, 0x31, 0x8C, 0x31, 0x86, 0x33, 0x19,
  0x8C, 0x63, 0x18, 0xC6, 0x70, 0x78, 0x77, 0x18, 0x7C, 0x06 };

const GFXglyph Helvetica_Normal12pt7bGlyphs[] PROGMEM = {
  {     0,   0,   0,   7,    0,    1 },   // 0x20 ' '
  {     0,   2,  18,   7,    3,  -17 },   // 0x21 '!'
  {     5,   7,   6,   9,    1,  -16 },   // 0x22 '"'
  {    11,  13,  17,  13,    0,  -16 },   // 0x23 '#'
  {    39,  12,  21,  13,    1,  -17 },   // 0x24 '$'
  {    71,  19,  18,  21,    1,  -16 },   // 0x25 '%'
  {   114,  15,  18,  16,    1,  -16 },   // 0x26 '&'
  {   148,   3,   6,   5,    1,  -16 },   // 0x27 '''
  {   151,   5,  23,   8,    2,  -17 },   // 0x28 '('
  {   166,   5,  23,   8,    1,  -17 },   // 0x29 ')'
  {   181,   7,   7,   9,    1,  -17 },   // 0x2A '*'
  {   188,  12,  12,  14,    1,  -11 },   // 0x2B '+'
  {   206,   3,   7,   7,    2,   -2 },   // 0x2C ','
  {   209,   6,   2,   8,    1,   -7 },   // 0x2D '-'
  {   211,   3,   3,   7,    2,   -2 },   // 0x2E '.'
  {   213,   7,  17,   7,    0,  -16 },   // 0x2F '/'
  {   228,  11,  18,  13,    1,  -16 },   // 0x30 '0'
  {   253,   6,  17,  13,    2,  -16 },   // 0x31 '1'
  {   266,  12,  17,  13,    1,  -16 },   // 0x32 '2'
  {   292,  11,  18,  13,    1,  -16 },   // 0x33 '3'
  {   317,  12,  17,  13,    1,  -16 },   // 0x34 '4'
  {   343,  12,  18,  13,    1,  -16 },   // 0x35 '5'
  {   370,  11,  18,  13,    1,  -16 },   // 0x36 '6'
  {   395,  12,  17,  13,    1,  -16 },   // 0x37 '7'
  {   421,  11,  18,  13,    1,  -16 },   // 0x38 '8'
  {   446,  11,  18,  13,    1,  -16 },   // 0x39 '9'
  {   471,   3,  13,   7,    2,  -12 },   // 0x3A ':'
  {   476,   3,  16,   7,    2,  -11 },   // 0x3B ';'
  {   482,  12,  11,  14,    1,  -10 },   // 0x3C '<'
  {   499,  12,   6,  14,    1,   -8 },   // 0x3D '='
  {   508,  12,  11,  14,    1,  -10 },   // 0x3E '>'
  {   525,  10,  18,  13,    2,  -17 },   // 0x3F '?'
  {   548,  22,  22,  24,    1,  -17 },   // 0x40 '@'
  {   609,  16,  18,  16,    0,  -17 },   // 0x41 'A'
  {   645,  13,  18,  16,    2,  -17 },   // 0x42 'B'
  {   675,  15,  19,  17,    1,  -17 },   // 0x43 'C'
  {   711,  14,  18,  17,    2,  -17 },   // 0x44 'D'
  {   743,  13,  18,  16,    2,  -17 },   // 0x45 'E'
  {   773,  12,  18,  15,    2,  -17 },   // 0x46 'F'
  {   800,  16,  18,  19,    1,  -17 },   // 0x47 'G'
  {   836,  13,  18,  17,    2,  -17 },   // 0x48 'H'
  {   866,   3,  18,   7,    2,  -17 },   // 0x49 'I'
  {   873,  10,  18,  12,    0,  -17 },   // 0x4A 'J'
  {   896,  14,  18,  16,    2,  -17 },   // 0x4B 'K'
  {   928,  11,  18,  13,    2,  -17 },   // 0x4C 'L'
  {   953,  17,  18,  20,    2,  -17 },   // 0x4D 'M'
  {   992,  14,  18,  17,    2,  -17 },   // 0x4E 'N'
  {  1024,  17,  19,  19,    1,  -17 },   // 0x4F 'O'
  {  1065,  13,  18,  16,    2,  -17 },   // 0x50 'P'
  {  1095,  17,  19,  19,    1,  -17 },   // 0x51 'Q'
  {  1136,  14,  18,  17,    2,  -17 },   // 0x52 'R'
  {  1168,  14,  19,  16,    1,  -17 },   // 0x53 'S'
  {  1202,  14,  18,  15,    0,  -17 },   // 0x54 'T'
  {  1234,  14,  19,  17,    2,  -17 },   // 0x55 'U'
  {  1268,  15,  18,  16,    1,  -17 },   // 0x56 'V'
  {  1302,  21,  18,  23,    1,  -17 },   // 0x57 'W'
  {  1350,  15,  18,  16,    1,  -17 },   // 0x58 'X'
  {  1384,  16,  18,  16,    0,  -17 },   // 0x59 'Y'
  {  1420,  13,  18,  15,    1,  -17 },   // 0x5A 'Z'
  {  1450,   4,  23,   7,    2,  -17 },   // 0x5B '['
  {  1462,   7,  19,   7,    0,  -17 },   // 0x5C '\'
  {  1479,   4,  23,   7,    1,  -17 },   // 0x5D ']'
  {  1491,  10,   9,  11,    1,  -16 },   // 0x5E '^'
  {  1503,  15,   1,  13,   -1,    4 },   // 0x5F '_'
  {  1505,   5,   4,   8,    1,  -17 },   // 0x60 '`'
  {  1508,  12,  13,  13,    1,  -12 },   // 0x61 'a'
  {  1528,  12,  19,  13,    1,  -17 },   // 0x62 'b'
  {  1557,  11,  14,  12,    1,  -12 },   // 0x63 'c'
  {  1577,  12,  19,  13,    0,  -17 },   // 0x64 'd'
  {  1606,  11,  13,  13,    1,  -12 },   // 0x65 'e'
  {  1624,   6,  18,   7,    0,  -17 },   // 0x66 'f'
  {  1638,  11,  18,  13,    1,  -12 },   // 0x67 'g'
  {  1663,  10,  18,  13,    2,  -17 },   // 0x68 'h'
  {  1686,   2,  18,   5,    2,  -17 },   // 0x69 'i'
  {  1691,   5,  23,   5,   -1,  -17 },   // 0x6A 'j'
  {  1706,  11,  18,  12,    1,  -17 },   // 0x6B 'k'
  {  1731,   2,  18,   5,    2,  -17 },   // 0x6C 'l'
  {  1736,  16,  13,  20,    2,  -12 },   // 0x6D 'm'
  {  1762,  10,  13,  13,    2,  -12 },   // 0x6E 'n'
  {  1779,  11,  13,  13,    1,  -12 },   // 0x6F 'o'
  {  1797,  12,  18,  13,    1,  -12 },   // 0x70 'p'
  {  1824,  11,  18,  13,    1,  -12 },   // 0x71 'q'
  {  1849,   6,  13,   8,    2,  -12 },   // 0x72 'r'
  {  1859,  10,  14,  12,    1,  -12 },   // 0x73 's'
  {  1877,   6,  16,   7,    0,  -15 },   // 0x74 't'
  {  1889,  10,  14,  13,    2,  -12 },   // 0x75 'u'
  {  1907,  12,  13,  12,    0,  -12 },   // 0x76 'v'
  {  1927,  17,  13,  17,    0,  -12 },   // 0x77 'w'
  {  1955,  12,  13,  12,    0,  -12 },   // 0x78 'x'
  {  1975,  12,  18,  12,    0,  -12 },   // 0x79 'y'
  {  2002,  10,  13,  12,    1,  -12 },   // 0x7A 'z'
  {  2019,   6,  23,   8,    1,  -17 },   // 0x7B '{'
  {  2037,   2,  22,   6,    2,  -16 },   // 0x7C '|'
  {  2043,   5,  22,   8,    1,  -16 },   // 0x7D '}'
  {  2057,  10,   4,  14,    2,   -9 } }; // 0x7E '~'

const GFXfont Helvetica_Normal12pt7b PROGMEM = {
  (uint8_t  *)Helvetica_Normal12pt7bBitmaps,
  (GFXglyph *)Helvetica_Normal12pt7bGlyphs,
  0x20, 0x7E, 28 };

// Approx. 2734 bytes
