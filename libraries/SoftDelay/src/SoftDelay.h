// SoftDelay.h

#ifndef _SoftDelay_h
#define _SoftDelay_h

//#if defined(ARDUINO) && ARDUINO >= 100
//#include "arduino.h"
//#else
//#include "WProgram.h"
//#endif

#include <Arduino.h>

class SoftDelay
{
public:

	unsigned long PreviousMillis = 0;
	SoftDelay() {

	}

	void begin(long interval, bool oneTime = false);
	bool isTime();

private:

	unsigned int _interval = 0;
	bool _oneTime = false;
	bool _ticked = false;
};


inline void SoftDelay::begin(long interval, bool oneTime)
{
	_interval = interval;
	PreviousMillis = millis();
	_oneTime = oneTime;
	_ticked = false;
}

inline bool SoftDelay::isTime()
{
	if(_oneTime && _ticked) {
		return false;
	}

	if ((millis() - PreviousMillis) > _interval) {
		PreviousMillis = millis();
		_ticked = true;
		return true;
	}
	else
	{
		return false;
	}
}


#endif

