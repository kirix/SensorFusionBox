#ifndef _BoxLogging_h
#define _BoxLogging_h

#include <WiFiUdp.h>

extern "C" {
	#include "user_interface.h"
}

class BoxLogging {

public:

	typedef enum  {
		OFF, FATAL, ERROR, WARN, INFO, DEBUG, TRACE, ALL
	} LOG_LEVEL;

	int SendUdpPort = 8266;
	int ListenUdpPort = 0;
	String IpAddress = "";
	WiFiUDP Udp;
	unsigned long udpRetryingTimeMillis = 15000;
	bool udpDebug = false;
	LOG_LEVEL LogLevel;

	BoxLogging() {
	}

	BoxLogging(LOG_LEVEL logLevel) {
		LogLevel = logLevel;
	}

	void udpBegin(int listenUdpPort) {
		ListenUdpPort = listenUdpPort;

		Udp.begin(ListenUdpPort);
		Serial.println("Start listening for Udp Debug IPAddress on port: " + String(ListenUdpPort));

		uint32_t beginWait = millis();
		while (millis() - beginWait < udpRetryingTimeMillis) {
			if(_hasUdpServerSendIP()) {
				udpDebug = true;
				println(LOG_LEVEL::INFO, "Hello World!");
				return;
			}
			delay(50);
			Serial.print(".");
		}

		Udp.stop();
		Serial.println();
    	Serial.println("No Udp Debug IPAddress!!! Debugging to Serial...");
	}

	void println(LOG_LEVEL logLevel, String text = "") {
		if(LogLevel >= logLevel) {
			_print(text += '\n');
		}
	}

	void print(LOG_LEVEL logLevel, String text) {
		if(logLevel >= LogLevel) {
			_print(text);
		}
	}

	void hasDebugMessage() {
		if(!udpDebug) {
			if(_hasUdpServerSendIP()) {
				udpDebug = true;
			}
		}
	}

	void setLogLevel(LOG_LEVEL logLevel) {
		LogLevel = logLevel;
	}

private:
	int _NtpPacketSize = 48;
	char _PacketBuffer[48]; //buffer to hold incoming and outgoing packets

    void _print(String text) {
    	if(udpDebug) {
			Udp.beginPacket(IpAddress.c_str(), SendUdpPort); //NTP requests are to port 123
			Udp.write((const uint8_t*)text.c_str(), text.length());
			Udp.endPacket();
    	}
    	else {
    		Serial.print(text);
    	}
	}

    bool _hasUdpServerSendIP() {

		//while (Udp.parsePacket() > 0); // discard any previously received packets

		if(Udp.parsePacket() > 0) {
			Udp.read(_PacketBuffer, _NtpPacketSize);
			String text = String(_PacketBuffer);
			Serial.println();
			Serial.println(text);
			if(text.startsWith("debugIP")) {
				Serial.println("startsWith debugIP");
				IpAddress = text.substring(8);
				Serial.println("IPAddress set: " + IpAddress);
				if(SendUdpPort > 0) {
					println(LOG_LEVEL::INFO, "IPAddress set!");
					return true;
				}
			}
			if(text.startsWith("debugPort")) {
				Serial.println("startsWith debugPort");
				SendUdpPort = text.substring(10).toInt();
				Serial.println("SendUdpPort set: " + SendUdpPort);
				if(IpAddress != "") {
					println(LOG_LEVEL::INFO, "SendUdpPort set!");
					return true;
				}
			}
		}
    	return false;
    }
};


#endif

