/*
 * BoxWeMosCommons.h
 *
 *  Created on: Jul 24, 2016
 *      Author: kirikaki
 */

#ifndef BOXWEMOSCOMMONS_H_
#define BOXWEMOSCOMMONS_H_

#include <SoftwareSerial.h>
#include <BoxLogging.h>
#include <BoxSDStorage.h>
#include <ESP8266WiFi.h>
#include <vector>

SoftwareSerial SoftWareSerial(D1, D2, false, 256);

BoxLogging Logging = BoxLogging(BoxLogging::ALL);

//This is the library handling all SD requests
const int SDSSPin = 15;
BoxSDStorage SDStorage = BoxSDStorage(&Logging, SDSSPin);

char* SSID_STA = "Maya-Net";
char* WiFiPass_STA = "lolitatoumba";
const String WiFiStatus[] = {
	"IDLE STATUS", "NO SSID AVAIL", "SCAN COMPLETED", "CONNECTED", "CONNECT FAILED", "CONNECTION LOST", "DISCONNECTED"
};

typedef enum {
	WifiStatusNone, WifiStatusStored, WifiStatusDisconnected, WifiStatusConnected
} WifiStatus;

struct WifiNetwork {
	String SSID;
	int Signal;
	WifiStatus Status;
	String Pass;
};

std::vector<WifiNetwork> WifiNetworks;

typedef std::function<void(int)> SentCallback;

SentCallback sendCallback = NULL;

unsigned int sendCount;

unsigned int sendIndex;

#endif /* BOXWEMOSCOMMONS_H_ */

