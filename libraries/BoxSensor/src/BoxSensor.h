#ifndef _BoxSensors_h
#define _BoxSensors_h

#include <DHT.h>

const String SensorTypes[] = { "BoxSensorUnknown", "BoxSensorDHT", "BoxSensorSoil1", "BoxSensorSoil2" };

typedef enum {
	BoxSensorUnknown, BoxSensorDHT, BoxSensorSoil1, BoxSensorSoil2
} BoxSensorType;

class BoxSensor {
public:
	int Id;
	int DigitalPin;
	int AnalogPin;
	String AnalogPinAlias;
	BoxSensorType SensorType = BoxSensorUnknown;
	float PrimaryValue = 0;
	float SecondaryValue = 0;
	float AnalogValue = 0;

	BoxSensor() {}

	BoxSensor(int id, int digitalPin, int analogPin, String analogPinAlias, volatile bool* HasInterrupt) {
		Id = id;
		DigitalPin = digitalPin;
		AnalogPin = analogPin;
		AnalogPinAlias = analogPinAlias;
		SensorType = BoxSensorUnknown;
		hasInterrupt = HasInterrupt;
		pinMode(digitalPin, INPUT_PULLUP);
	}

	BoxSensorType checkType() {
		if(SensorType == BoxSensorUnknown) {
			//Check for DHT sensor
			_DHTSensor = DHT(DigitalPin, DHT22);
			_DHTSensor.begin();
			float vlaue = _DHTSensor.readHumidity();
			digitalRead(DigitalPin);
			analogRead(AnalogPin);

			if (!isnan(vlaue)) {
				SensorType = BoxSensorDHT;
			}
			else if(digitalRead(DigitalPin) == 1 && analogRead(AnalogPin) > 10) {
				SensorType = BoxSensorSoil1;
			}
			else if(digitalRead(DigitalPin) == 0 && analogRead(AnalogPin) < 10) {
				SensorType = BoxSensorSoil2;
			}
			else {
				SensorType = BoxSensorUnknown;
			}
			return SensorType;
		}
	}

	float getPrimaryValue() {
		switch (SensorType)
		{
			case BoxSensorDHT:
				PrimaryValue = _DHTSensor.readTemperature();
				if (isnan(PrimaryValue)) {
					SensorType = BoxSensorUnknown;
				}
				return PrimaryValue;
				break;
			case BoxSensorUnknown:
				digitalRead(DigitalPin);
				digitalRead(DigitalPin);
				return (float)(digitalRead(DigitalPin) * 1.0);
				break;
			default:
				return -1;
				break;
		}
	}

	float getSecondaryValue() {
		switch (SensorType)
		{
			case BoxSensorDHT:
				SecondaryValue = _DHTSensor.readHumidity();
				if (isnan(SecondaryValue)) {
					SensorType = BoxSensorUnknown;
				}
				return SecondaryValue;
				break;
			case BoxSensorUnknown:
				return -1;
			default:
				return -1;
				break;
		}
	}

	float getAnalogValue() {
		analogRead(AnalogPin);
		analogRead(AnalogPin);
		AnalogValue = analogRead(AnalogPin);

		switch(SensorType) {
			case BoxSensorSoil1:
				AnalogValue = fabs(AnalogValue);
				AnalogValue = fabs(((AnalogValue - 994) / (420 - 994)) * 100);
				if(AnalogValue > 100) AnalogValue = 100;
			break;
			case BoxSensorSoil2:
				AnalogValue = fabs(AnalogValue);
				AnalogValue = fabs(((AnalogValue - 0) / (850 - 0)) * 100);
				if(AnalogValue > 100) AnalogValue = 100;
			break;
		}

		return AnalogValue;
	}

private:
	DHT _DHTSensor = DHT(0, DHT22);
	volatile bool* hasInterrupt;

	float thermistorValue(int RawADC) {
		Serial.print(RawADC);
		float Temp;
		Temp = log(10000.0*((1024.0 / RawADC - 1)));
		Temp = 1 / (0.001129148 + (0.000234125 + (0.0000000876741 * Temp * Temp))* Temp);
		Temp = Temp - 273.15;            // Convert Kelvin to Celcius
		//Temp = (Temp * 9.0) / 5.0 + 32.0; // Convert Celcius to Fahrenheit
		return Temp;
	}
};

class BoxSensorsClass {
public:

	BoxSensorsClass(int digitalPins[], int analogPins[], String analogPinAlias[], int sensorCount, volatile bool* hasInterrupt) {
		Count = sensorCount;
		BoxSensors = new BoxSensor*[Count];

		for (int i = 0; i < Count; i++) {
			BoxSensors[i] = new BoxSensor(i, digitalPins[i], analogPins[i], analogPinAlias[i], hasInterrupt);
		}

		Serial.print(F("BoxSensorsClass initialized, Count:"));
		Serial.println(Count);
	}

	BoxSensor **BoxSensors;
	int Count;

private:

};

#endif

