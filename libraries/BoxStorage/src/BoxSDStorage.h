#ifndef _BoxSDStorage_h
#define _BoxSDStorage_h

#include <SPI.h>
#include <SD.h>
#include <BoxLogging.h>
#include <ArduinoJson.h>

class BoxSDStorage {

public:

	int SSPin;

	BoxSDStorage(BoxLogging* logging, int sdSSPin) {
		Logging = logging;
		SSPin = sdSSPin;
	}

	bool begin() {
		Logging->println(BoxLogging::INFO, "Starting SD card, SS pin: " + String(SSPin));
		if(!SD.begin(SSPin)) {
			Logging->println(BoxLogging::FATAL, "Cannot begin SD!!!");
			return false;
		}
		Logging->println(BoxLogging::INFO, "SD card started");
		return true;
	}

	void getWiFiConfigData(char **wifiSSID, char **wifiPASS) {
		if(!SD.exists("config")) {
			Logging->println(BoxLogging::FATAL, "config folder don't exist!!!");
		}
		else {
			Logging->println(BoxLogging::DEBUG, "config folder exists.");
			String wifiDataFile = "config/wifiData";
			if(SD.exists((char*)wifiDataFile.c_str())) {
				Logging->println(BoxLogging::DEBUG, "wifiData file exists.");
				String fileData = _readFile(wifiDataFile);
//				Logging->println(BoxLogging::DEBUG, "wifiDataFile Data:");
//				Logging->println(BoxLogging::DEBUG, fileData);
				if(fileData != "") {
					DynamicJsonBuffer jsonBuffer;
					JsonObject& jsonWifiData = jsonBuffer.parseObject((const char*)fileData.c_str(), 15);
					if(jsonWifiData.success()) {
//						Logging->println(BoxLogging::DEBUG, "wifiDataFile json parsing succeeded");
						if(jsonWifiData.containsKey("wifiConfig")) {
//							Logging->println(BoxLogging::DEBUG, "wifiDataFile containsKey: wifiConfig");
							JsonObject& jsonWifiConfig = jsonWifiData.get("wifiConfig");
							if(jsonWifiConfig.containsKey("wifiSSID")) {
//								Logging->println(BoxLogging::DEBUG, "wifiDataFile containsKey: wifiSSID");
								*wifiSSID = (char*)jsonWifiConfig.get("wifiSSID").asString();
								if(jsonWifiConfig.containsKey("wifiPASS")) {
//									Logging->println(BoxLogging::DEBUG, "wifiDataFile containsKey: wifiPASS");
									*wifiPASS = (char*)jsonWifiConfig.get("wifiPASS").asString();
								}
							}
						}
					}
					else {
						Logging->println(BoxLogging::ERROR, "wifiDataFile json parsing fail!!!");
						Logging->println(BoxLogging::ERROR, "wifiDataFile Data:");
						Logging->println(BoxLogging::ERROR, fileData);
					}
				}
				else {
					Logging->println(BoxLogging::ERROR, "wifiDataFile is Empty!!!");
					return;
				}
			}
			else {
				Logging->println(BoxLogging::WARN, "wifiData file don't exist!!! Creating...");
				StaticJsonBuffer<500> jsonSensorsBuffer;
				JsonObject& root = jsonSensorsBuffer.createObject();
				JsonObject& jsonWiFiConfig = root.createNestedObject("wifiConfig");
				jsonWiFiConfig["wifiSSID"] = *wifiSSID;
				jsonWiFiConfig["wifiPASS"] = *wifiPASS;
				String wifiData = "";
				jsonWiFiConfig.prettyPrintTo(wifiData);
				_writeFile(wifiDataFile, wifiData);
			}
		}
	}

private:
	BoxLogging* Logging;
	File myFile;

	String  _readFile(String fileToRead) {
		Logging->println(BoxLogging::TRACE, "SD opening file: " + fileToRead);
		myFile = SD.open(fileToRead);
		if (myFile) {
			Logging->println(BoxLogging::TRACE, "SD file: " + fileToRead + " opened.");
			String fileData = "";
			// read from the file until there's nothing else in it:
			while (myFile.available()) {
				fileData += (char)myFile.read();
			}

			return fileData;
			// close the file:
			myFile.close();
		} else {
			// if the file didn't open, print an error:
			Logging->println(BoxLogging::ERROR, "SD error opening " + fileToRead);
			return "";
		}
	}

	bool _writeFile(String fileToWrite, String data) {
		// open the file. note that only one file can be open at a time,
		// so you have to close this one before opening another.
		Logging->println(BoxLogging::TRACE, "SD writing file: " + fileToWrite);
		myFile = SD.open(fileToWrite, FILE_WRITE);
		// if the file opened okay, write to it:
		if (myFile) {
			Logging->println(BoxLogging::DEBUG, "SD Writing to: " + fileToWrite + "...");
			myFile.println(data);
			// close the file:
			myFile.close();
			Logging->println(BoxLogging::TRACE, "done.");
		} else {
			// if the file didn't open, print an error:
			Logging->println(BoxLogging::ERROR, "SD error opening " + fileToWrite);
		}
	}

};


#endif

