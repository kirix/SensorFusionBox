/*
 Name:		DisplayMqttSerial.ino
 Created:	4/9/2016 2:27:12 PM
 Author:	kirikaki
*/

#include <SoftDelay.h>
#include <BoxWeMosCommons.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>
#include <TimeLib.h>
#include <BoxWeMosMegaIf.h>

extern "C" {
	#include "user_interface.h"
}

const char* mqtt_server = "192.168.2.7";
const char * OTAPass = "//Colas1";

WiFiClient espClient;
PubSubClient mqttClient(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

char inputString[1600];         // a string to hold incoming data
int inputStringIndex = 0;

boolean stringComplete = false;  // whether the string is complete

SoftDelay reconnectNTPDelay;

unsigned int localPort = 2390;      // local port to listen for UDP packets

									/* Don't hardwire the IP address or we won't get the benefits of the pool.
									*  Lookup the IP address for the host name instead */
									//IPAddress timeServer(129, 6, 15, 28); // time.nist.gov NTP server
IPAddress timeServerIP; // time.nist.gov NTP server address
const char* ntpServerName = "time.nist.gov";

const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message

byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets

									// A UDP instance to let us send and receive packets over UDP
WiFiUDP Udp;

const int timeZone = 3;     // Eastern European Time
							//const int timeZone = -5;  // Eastern Standard Time (USA)
							//const int timeZone = -4;  // Eastern Daylight Time (USA)
							//const int timeZone = -8;  // Pacific Standard Time (USA)
							//const int timeZone = -7;  // Pacific Daylight Time (USA)

time_t lastIRSensorTime;
bool lastIRSensorState = true;

BoxWeMosMegaIf MegaInterface =  BoxWeMosMegaIf();


void setup_wifi(char* ssid, char* password) {

	delay(10);
	// We start by connecting to a WiFi network
	Serial.println();
	Serial.print("Connecting to ");
	Serial.println(ssid);

	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, password);

	while (WiFi.status() != WL_CONNECTED) {
		delay(250);
		Serial.print(".");
	}

	Serial.println("");
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());
}

void setup_OTA() {

	//Port defaults to 8266
	//ArduinoOTA.setPort(8266);

	//Hostname defaults to esp8266-[ChipID]
	ArduinoOTA.setHostname("BoxWeMos");

	ArduinoOTA.setPassword(OTAPass);

	ArduinoOTA.onStart([]() {
		Logging.println(BoxLogging::INFO, "OTA Start");
		Logging.println(BoxLogging::INFO, "Progress: 0%");
	});
	ArduinoOTA.onEnd([]() {
		Logging.println(BoxLogging::INFO, "OTA End");
	});
	ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
		Logging.print(BoxLogging::INFO, String(progress / (total / 100)) + "%");
	});
	ArduinoOTA.onError([](ota_error_t error) {
		Logging.println(BoxLogging::ERROR, "Error " + String(error));
		if (error == OTA_AUTH_ERROR) Logging.println(BoxLogging::ERROR, "Auth Failed");
		else if (error == OTA_BEGIN_ERROR) Logging.println(BoxLogging::ERROR, "Begin Failed");
		else if (error == OTA_CONNECT_ERROR) Logging.println(BoxLogging::ERROR, "Connect Failed");
		else if (error == OTA_RECEIVE_ERROR) Logging.println(BoxLogging::ERROR, "Receive Failed");
		else if (error == OTA_END_ERROR) Logging.println(BoxLogging::ERROR, "End Failed");
	});

	ArduinoOTA.begin();
	Logging.println(BoxLogging::INFO, "OTA Ready");
}

void setup_NTP() {

	Udp.begin(localPort);
	setSyncProvider(getNtpTime);
	reconnectNTPDelay.begin(2000);
}

// the setup function runs once when you press reset or power the board
void setup() {
	delay(100);

	Serial.begin(115200);
	SoftWareSerial.begin(57600);
	//Esp8266 eats the first occurrence;
	Serial.println();

	Serial.println("BoxWeMos staring...");

	if(SDStorage.begin()) {
		SDStorage.getWiFiConfigData(&SSID_STA, &WiFiPass_STA);
		Logging.println(BoxLogging::INFO, String(SSID_STA) + " " + String(WiFiPass_STA));
	}
	Logging.println(BoxLogging::DEBUG, "Free Memory: " + String(system_get_free_heap_size()));

	setup_wifi(SSID_STA, WiFiPass_STA);
	setup_OTA();

	Logging.udpBegin(8267);

	setup_NTP();

	//mqttClient.setServer(mqtt_server, 1883);
	//mqttClient.setCallback(callback);


	lastIRSensorTime = now();
	delay(500);
}

// the loop function runs over and over again until power down or reset
void loop() {
	if (SoftWareSerial.available()) {
		serialEvent();
	}

	/*if (!mqttClient.connected()) {
		reconnectMqtt();
	}*/
	//mqttClient.loop();

	if (timeStatus() == timeNotSet) {
		if(reconnectNTPDelay.isTime()) {
			setSyncProvider(getNtpTime);
		}
	}

	if(stringComplete) {
		stringComplete = false;

		MegaInterface.proccesSerialData(String(inputString));

		int i = 0;
		//fill the rest of the buffer with nulls
		while(i < 1599) {
			inputString[i] = '\0';
			i++;
		}
	}

	ArduinoOTA.handle();
}

void serialEvent() {
	while (SoftWareSerial.available()) {
		// get the new byte:
		char inChar = (char)SoftWareSerial.read();
		// add it to the inputString:
		inputString[inputStringIndex] = inChar;
		inputStringIndex++;
		// if the incoming character is a newline, set a flag
		// so the main loop can do something about it:
		if (inChar == '\n') {
		  stringComplete = true;
		  inputStringIndex = 0;
		}
	}
	//fill the rest of the buffer with nulls
//	while(i < 1599) {
//		inputString[i] = '0';
//		i++;
//	}
}

void callback(char* topic, byte* payload, unsigned int length) {
	Logging.print(BoxLogging::DEBUG, "Message arrived [");
	Logging.print(BoxLogging::DEBUG, topic);
	Logging.print(BoxLogging::DEBUG, "] ");
	for (int i = 0; i < length; i++) {
		Logging.print(BoxLogging::DEBUG, (String)payload[i]);
	}
	Logging.println(BoxLogging::DEBUG);
}

void reconnectMqtt() {
	// Loop until we're reconnected
	while (!mqttClient.connected()) {
		Logging.print(BoxLogging::INFO, "Attempting MQTT connection...");
		// Attempt to connect
		if (mqttClient.connect("ESP8266Client")) {
			Logging.println(BoxLogging::INFO, "connected");
			// Once connected, publish an announcement...
			mqttClient.publish("outTopic", "hello world");
			// ... and resubscribe
			mqttClient.subscribe("inTopic");
		}
		else {
			Logging.print(BoxLogging::WARN, "failed, rc=");
			Logging.print(BoxLogging::WARN, String(mqttClient.state()));
			Logging.println(BoxLogging::INFO, " try again in 5 seconds");
			// Wait 5 seconds before retrying
			delay(5000);
		}
	}
}

void digitalClockDisplay() {
	// digital clock display of the time
	Serial.print(hour());
	printDigits(minute());
	printDigits(second());
	Serial.print(" ");
	Serial.print(day());
	Serial.print(".");
	Serial.print(month());
	Serial.print(".");
	Serial.print(year());
	Serial.println();
}

void printDigits(int digits) {
	// utility for digital clock display: prints preceding colon and leading 0
	Serial.print(":");
	if (digits < 10)
		Serial.print('0');
	Serial.print(digits);
}

time_t getNtpTime()
{
	//get a random server from the pool
	WiFi.hostByName(ntpServerName, timeServerIP);
	while (Udp.parsePacket() > 0); // discard any previously received packets
	Logging.println(BoxLogging::INFO, "Transmit NTP Request");
	sendNTPpacket(timeServerIP);
	uint32_t beginWait = millis();
	while (millis() - beginWait < 1500) {
		int size = Udp.parsePacket();
		if (size >= NTP_PACKET_SIZE) {
			Logging.println(BoxLogging::INFO, "Receive NTP Response");
			Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
			unsigned long secsSince1900;
			// convert four bytes starting at location 40 to a long integer
			secsSince1900 = (unsigned long)packetBuffer[40] << 24;
			secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
			secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
			secsSince1900 |= (unsigned long)packetBuffer[43];
			return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
		}
	}
	Logging.println(BoxLogging::WARN, "No NTP Response :-(");
	return 0; // return 0 if unable to get the time
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress &address)
{
	// set all bytes in the buffer to 0
	memset(packetBuffer, 0, NTP_PACKET_SIZE);
	// Initialize values needed to form NTP request
	// (see URL above for details on the packets)
	packetBuffer[0] = 0b11100011;   // LI, Version, Mode
	packetBuffer[1] = 0;     // Stratum, or type of clock
	packetBuffer[2] = 6;     // Polling Interval
	packetBuffer[3] = 0xEC;  // Peer Clock Precision
							 // 8 bytes of zero for Root Delay & Root Dispersion
	packetBuffer[12] = 49;
	packetBuffer[13] = 0x4E;
	packetBuffer[14] = 49;
	packetBuffer[15] = 52;
	// all NTP fields have been given values, now
	// you can send a packet requesting a timestamp:
	Udp.beginPacket(address, 123); //NTP requests are to port 123
	Udp.write(packetBuffer, NTP_PACKET_SIZE);
	Udp.endPacket();
}
