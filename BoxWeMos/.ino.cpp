#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2017-03-06 13:40:21

#include "Arduino.h"
#include <SoftDelay.h>
#include <BoxWeMosCommons.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>
#include <TimeLib.h>
#include <BoxWeMosMegaIf.h>
#include "user_interface.h"
void setup_wifi(char* ssid, char* password) ;
void setup_OTA() ;
void setup_NTP() ;
void setup() ;
void loop() ;
void serialEvent() ;
void callback(char* topic, byte* payload, unsigned int length) ;
void reconnectMqtt() ;
void digitalClockDisplay() ;
void printDigits(int digits) ;
time_t getNtpTime() ;
void sendNTPpacket(IPAddress &address) ;

#include "BoxWeMos.ino"


#endif
