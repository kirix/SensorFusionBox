/*
 Name:		Sketch1.ino
 Created:	4/9/2016 11:07:52 AM
 Author:	kirikaki
*/

#include <DHT.h>
#include <SoftDelay.h>
#include <math.h>
#include <BoxDisplayClass.h>
#include <BoxMegaWeMosIf.h>
#include <BoxSensor.h>


unsigned int IntervalSensorPolling = 400;
unsigned int IntervalPrinting = 3000;
unsigned int IntervalCheckActions = 50;
unsigned int IntervalInterrupt = 2000;
unsigned int IntervalInactivity = 30000;
unsigned int LongButtonPressTime = 1000;//MUST BE LESS THAN IntervalInterrupt
unsigned int ShortButtonPressTime = 25;//MUST BE LESS THAN IntervalInterrupt
SoftDelay DelaySensorPolling;
SoftDelay DelayPrinting;
SoftDelay DelayCheckActions;
SoftDelay DelayInterrupt;
SoftDelay DelayInactivity;

//Sensor IR PIN
#define sensorIRpin 2

//Rotary Encoder PIN's definition
int encoderPin1 = 21;
int encoderPin2 = 31;
int encoderSwitchPin = 20; //push button switch
int backSwitchPin = 3;

volatile int lastEncoded = 0;
volatile long encoderValue = 0;
volatile bool HasLongInterrupt = false;
volatile bool HasShortInterrupt = false;

long lastEncoderValue = 0;

int lastMSB = 0;
int lastLSB = 0;

volatile bool encoderSwitchPressed = false;
volatile bool backSwitchPressed = false;
volatile bool sensorIRactive = false;

String inputString = "";         // a string to hold incoming data
int inputStringIndex = 0;
bool stringComplete = false;  // whether the string is complete
bool onSerial1Receive = false;

void setUpRotaryEncoder() {
	pinMode(encoderPin1, INPUT);
	pinMode(encoderPin2, INPUT);

	pinMode(encoderSwitchPin, INPUT);


	digitalWrite(encoderPin1, HIGH); //turn pullup resistor on
	digitalWrite(encoderPin2, HIGH); //turn pullup resistor on

	digitalWrite(encoderSwitchPin, HIGH); //turn pullup resistor on


	attachInterrupt(digitalPinToInterrupt(encoderPin1), interruptEncoder, CHANGE);
	attachInterrupt(digitalPinToInterrupt(encoderSwitchPin), interruptEncoderSwitch, CHANGE);
	Serial.println(F("RotaryEncoder initialized."));
}

void setUpBackSwitch() {
	pinMode(backSwitchPin, INPUT);
	digitalWrite(backSwitchPin, HIGH); //turn pullup resistor on
	attachInterrupt(digitalPinToInterrupt(backSwitchPin), interrupBackSwitch, CHANGE);
}

void setUpIRSensor() {
	pinMode(sensorIRpin, INPUT);
	attachInterrupt(digitalPinToInterrupt(sensorIRpin), handleIRSensor, CHANGE);
}

void handleIRSensor() {
	if(digitalRead(sensorIRpin) == 1) {
		sensorIRactive = true;
		//Serial.println("SENSOR  IR");
	}
}

void interruptEncoder(){
	int MSB = digitalRead(encoderPin1); //MSB = most significant bit
	int LSB = digitalRead(encoderPin2); //LSB = least significant bit

	int encoded = (MSB << 1) |LSB; //converting the 2 pin value to single number
	int sum  = (lastEncoded << 2) | encoded; //adding it to the previous encoded value

	if(sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011) encoderValue ++;
	if(sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000) encoderValue --;
	lastEncoded = encoded; //store this value for next time
	HasLongInterrupt = true;
	HasShortInterrupt = true;
}

void interruptEncoderSwitch() {
	delay(ShortButtonPressTime);
	if(digitalRead(encoderSwitchPin) == 0) {
		HasLongInterrupt = true;
		HasShortInterrupt = true;
		encoderSwitchPressed = true;
		DelayInterrupt.begin(IntervalInterrupt);
	}
}

void interrupBackSwitch() {
	delay(ShortButtonPressTime);
	if(digitalRead(backSwitchPin) == 0) {
		HasLongInterrupt = true;
		HasShortInterrupt = true;
		backSwitchPressed = true;
		DelayInterrupt.begin(IntervalInterrupt);
	}
}

//Setup BoxSensors
int DigitalPins[] = { 32, 33, 34, 35, 36, 37, 39, 41,  43,  45,  47,  49 };
int AnalogPins[] =  { A0, A1, A2, A3, A4, A5, A8, A9, A10, A11, A12, A13 };
String AnalogPinAlias[] =  { "A0", "A1", "A2", "A3", "A4", "A5", "A8", "A9", "A10", "A11", "A12", "A13" };
int SensorsCount = 0;
int PollingSensor = 0;

BoxSensorsClass *boxSensors;

void setupBoxSensors() {
	int analogPinCount = sizeof(AnalogPins) / sizeof(int);
	int digitalPinCount = sizeof(DigitalPins) / sizeof(int);
	if(analogPinCount != digitalPinCount) {
		Serial.println(F("Error! > digitalPins != analogPins"));
		return;
	}
	SensorsCount = analogPinCount;
	boxSensors = new BoxSensorsClass(DigitalPins, AnalogPins, AnalogPinAlias, SensorsCount, &HasLongInterrupt);
}

BoxDisplayClass *boxDisplay;

// the setup function runs once when you press reset or power the board
void setup() {
	delay(100);

	Serial.begin(115200);
	Serial1.begin(115200);
	Serial1.setTimeout(25);

	DelaySensorPolling.begin(IntervalSensorPolling);
	DelayPrinting.begin(IntervalPrinting);
	DelayCheckActions.begin(IntervalCheckActions);
	DelayInterrupt.begin(IntervalInterrupt);
	DelayInactivity.begin(IntervalInactivity);

	setupBoxSensors();

	setUpRotaryEncoder();

	setUpIRSensor();

	//using namespace BoxScreenSectors;
	//using namespace SettingsSectors;
	setUpBackSwitch();

	boxDisplay = new BoxDisplayClass(boxSensors, &HasShortInterrupt);

	// reserve 200 bytes for the inputString:
	inputString.reserve(300);

	Serial.print(F("Free Memory: "));
	Serial.println(freeRam());
}

uint8_t buffer[63];
size_t message_length;
bool status;

// the loop function runs over and over again until power down or reset
void loop() {


	if(!onSerial1Receive) {
		checkDisplayActivity();

		if(DelayCheckActions.isTime() && HasLongInterrupt) {
			checkActions();
		}
		else if (DelaySensorPolling.isTime() && !HasLongInterrupt) {
			PollingSensor = getSensor(PollingSensor, boxSensors);
			//if(HasInterrupt) return;
		}
		else if (DelayPrinting.isTime()) {//&& !HasLongInterrupt) {
	//		Serial1.flush();
	//		Serial.println("Free Memory: ");
	//		Serial.println(freeRam());
			printSensors(boxSensors);
		}
	}

	size_t message_length = 0;

	if (Serial1.available()) {
		message_length = Serial1.read();
		//Serial.println("length = " +  String(message_length));
		if(message_length < 63) {
			Serial1.readBytes(buffer, message_length);
			/*for(uint8_t i = 0; i < message_length; i++) {
				buffer[i] = Serial1.read();
			}*/
			while(Serial1.available()) Serial1.read();

			BoxMegaWeMosIf::proccesSerialData(buffer, message_length);

			memset(&buffer[0], 0, sizeof(buffer));
		}
	}

//	if (Serial1.available()) {
//		noInterrupts();
//		while (Serial1.available()) {
//			onSerial1Receive = true;
//			char inChar = (char)Serial1.read();
//			inputString += inChar;
//			if (inChar == '\n') {
//				  stringComplete = true;
//				  //inputStringIndex = 0;
//				  interrupts();
//				  return;
//			}
//		}
//		interrupts();
//	}

//	// proccess Serial1 comm when a newline arrives:
//	if (stringComplete) {
		//BoxMegaWeMosComm.proccesSerialData(inputString);
//		// clear the string:
//		inputString = "";
//		stringComplete = false;
//		onSerial1Receive = false;
//	}

	if(!onSerial1Receive) {
		boxDisplay->updateScreen(PollingSensor);
	}

//	BoxMegaWeMosIf::update();
}

void checkActions() {
	bool changed = false;
	bool left = false;
	bool right = false;

	if(encoderValue > lastEncoderValue) {
		changed = true;
		right = true;
	}
	else if(encoderValue < lastEncoderValue) {
		changed = true;
		left = true;
	}
	lastEncoderValue = encoderValue;
	if(changed) {
		if(right) {
			boxDisplay->actionsOnScreen(BoxActionType::BoxActionRightShift);
			//Serial.println("encoder right");
		}
		else if(left) {
			boxDisplay->actionsOnScreen(BoxActionType::BoxActionLeftShift);
			//Serial.println("encoder left");
		}
	}

	if(encoderSwitchPressed) {
		boxDisplay->actionsOnScreen(BoxActionType::BoxActionRotarySwitch);
		encoderSwitchPressed = false;
		DelayInterrupt.begin(IntervalInterrupt);
	}

	if(backSwitchPressed) {
		boxDisplay->actionsOnScreen(BoxActionType::BoxActionBack);
		backSwitchPressed = false;
		DelayInterrupt.begin(IntervalInterrupt);
	}

	if(digitalRead(backSwitchPin) == 0 && ((millis() - DelayInterrupt.PreviousMillis) > LongButtonPressTime)) {
		boxDisplay->actionsOnScreen(BoxActionType::BoxActionBackLong);
	}

	if(DelayInterrupt.isTime()) {
		HasLongInterrupt = false;
	}
}

void checkDisplayActivity() {
	if(HasLongInterrupt || sensorIRactive) {
		if(boxDisplay->BOX_DISPLAY_ON) {
			DelayInactivity.begin(IntervalInactivity, true);
		}
		else {
			DelayInactivity.begin(IntervalInactivity, true);
			boxDisplay->onDisplay();
		}
		sensorIRactive = false;
	}
	else if(DelayInactivity.isTime()) {
		boxDisplay->offDisplay();
	}
}

int getSensor(int index, BoxSensorsClass *boxSensors) {
	boxSensors->BoxSensors[index]->getPrimaryValue();
	boxSensors->BoxSensors[index]->getSecondaryValue();
	boxSensors->BoxSensors[index]->getAnalogValue();
	boxSensors->BoxSensors[index]->checkType();

	index++;
	if(index >= boxSensors->Count) {
		index = 0;
	}
	return index;
}

void printSensors(BoxSensorsClass *boxSensors) {
//	if(HasInterrupt) return;
//	StaticJsonBuffer<1000> jsonSensorsBuffer;
//	JsonObject& root = jsonSensorsBuffer.createObject();
//	JsonObject& jsonBoxSensors = root.createNestedObject("boxSensors");
//
//	for (int i = 0; i < boxSensors->Count; i++) {
//		if(HasInterrupt) return;
//		JsonObject& json = jsonBoxSensors.createNestedObject(String(boxSensors->BoxSensors[i]->Id));
//		json["DigitalPin"] = boxSensors->BoxSensors[i]->DigitalPin;
//		json["AnalogPin"] = boxSensors->BoxSensors[i]->AnalogPinAlias;
//		json["Type"].set(SensorTypes[boxSensors->BoxSensors[i]->SensorType]);
//		json["PrimaryValue"].set(boxSensors->BoxSensors[i]->PrimaryValue);
//		json["SecValue"].set(boxSensors->BoxSensors[i]->SecondaryValue);
//		json["AnalogValue"].set(boxSensors->BoxSensors[i]->AnalogValue);
//	}
//	if(HasInterrupt) return;
//	root.printTo(Serial1);
//	Serial1.println();
//	if(HasInterrupt) return;
//	//root.prettyPrintTo(Serial);
//	Serial.println("Free Memory: ");
	Serial.println(freeRam());
}

int freeRam() {
  extern int __heap_start, *__brkval;
  int v;
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}

