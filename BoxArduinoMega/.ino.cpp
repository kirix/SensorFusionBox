#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2016-12-03 20:42:24

#include "Arduino.h"
#include <DHT.h>
#include <SoftDelay.h>
#include <math.h>
#include <BoxDisplayClass.h>
#include <BoxMegaWeMosIf.h>
#include <BoxSensor.h>
void setUpRotaryEncoder() ;
void setUpBackSwitch() ;
void setUpIRSensor() ;
void handleIRSensor() ;
void interruptEncoder();
void interruptEncoderSwitch() ;
void interrupBackSwitch() ;
void setupBoxSensors() ;
void setup() ;
void loop() ;
void checkActions() ;
void checkDisplayActivity() ;
int getSensor(int index, BoxSensorsClass *boxSensors) ;
void printSensors(BoxSensorsClass *boxSensors) ;
int freeRam() ;

#include "BoxArduinoMega.ino"


#endif
