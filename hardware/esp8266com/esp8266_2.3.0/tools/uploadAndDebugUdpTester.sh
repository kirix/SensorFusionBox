#!/bin/bash

echo "Tryning to get IP for BoxWeMos.local..."
boxIP=`getent ahosts BoxWeMos.local | grep 'BoxWeMos.local' | awk '{print $1}' | sed 's/addr://'`
echo "BoxWeMos.local IP: $boxIP"
if [[ $boxIP = *[!\ ]* ]]; then
  echo "Trying to upload Sketch on BoxWeMos.local"
  if result=$(python espota.py -i $boxIP -p 8266 --auth=//Colas1 -f ../../../../WeMosTester/Release/WeMosTester.bin); then
    echo 'Upload Finished Sucessfully!'
    echo 'Waiting for device to setup and connect to WiFi...'
    sleep 16
    boxIP=`getent ahosts BoxWeMos.local | grep 'BoxWeMos.local' | awk '{print $1}' | sed 's/addr://'`
    echo "BoxWeMos.local IP: $boxIP"

    wifiLocalIP=`ifconfig wlp2s0 | grep 'inet ' | awk '{print $2}' | sed 's/addr://'`
    echo "WiFi local IP : $wifiLocalIP"

    if [[ ($boxIP = *[!\ ]*) && ($wifiLocalIP = *[!\ ]*) ]]; then
      
      if result=$(echo -ne "debugIP $wifiLocalIP" |  nc -u BoxWeMos.local 8267);   then 
        echo 'IP Address send to Box'
        echo 'Start Listening on Udp 8266 for incoming debug messages...'
        nc -l -u 8266
      fi
    fi
  fi
fi
