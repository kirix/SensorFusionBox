

boxIP=`getent ahosts BoxWeMos.local | grep 'BoxWeMos.local' | awk '{print $1}' | sed 's/addr://'`
echo "BoxWeMos.local IP: $boxIP"

wifiLocalIP=`ifconfig wlo1 | grep 'inet ' | awk '{print $2}' | sed 's/addr://'`
echo "WiFi local IP : $wifiLocalIP"

if [[ ($boxIP = *[!\ ]*) && ($wifiLocalIP = *[!\ ]*) ]]; then

    if result=$(echo -ne "debugIP $wifiLocalIP" |  nc -u BoxWeMos.local 8267);   then
      echo 'IP Address send to Box'
      echo 'Start Listening on Udp 8266 for incoming debug messages...'
      nc -l -u 8266
    fi
fi

