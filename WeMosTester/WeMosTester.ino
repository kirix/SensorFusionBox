#include "Arduino.h"
#include <ArduinoOTA.h>
#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <SoftDelay.h>
#include <stdio.h>
#include <pb_encode.h>
#include <pb_decode.h>
#include "simple.pb.h"
#include <fileproto.pb.h>
#include <boxMegaWemos.pb.h>
#include <boxWemosMega.pb.h>

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

SoftwareSerial SoftWareSerial(D1, D2, false, 256);

WiFiUDP Udp;
unsigned long udpRetryingTimeMillis = 15000;
int _NtpPacketSize = 48;
char _PacketBuffer[48]; //buffer to hold incoming and outgoing packets
int SendUdpPort = 8266;
int ListenUdpPort = 0;
String IpAddress = "";
bool udpDebug = false;


SoftDelay SendTimeout;

void setup_wifi(char* ssid, char* password) {

	delay(10);
	// We start by connecting to a WiFi network
	//Serial.println();
	//Serial.print("Connecting to ");
	//Serial.println(ssid);

	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, password);

	while (WiFi.status() != WL_CONNECTED) {
		delay(250);
		//Serial.print(".");
	}

	//Serial.println("");
	//Serial.println("WiFi connected");
	//Serial.println("IP address: ");
	//Serial.println(WiFi.localIP());
}

void setup_OTA() {

	//Port defaults to 8266
	//ArduinoOTA.setPort(8266);

	//Hostname defaults to esp8266-[ChipID]
	ArduinoOTA.setHostname("BoxWeMos");

	ArduinoOTA.setPassword("//Colas1");

	ArduinoOTA.begin();
}

//The setup function is called once at startup of the sketch
void setup()
{
	Serial.begin(115200);
	SoftWareSerial.begin(115200);

	setup_wifi("Maya-Net", "lolitatoumba");
	setup_OTA();
	udpBegin(8267);

	inputString.reserve(200);

	SendTimeout.begin(200);
// Add your initialization code here
}
int itemsCount = 4;
int itemsSent = 0;
bool finished = false;
String Request = "";
bool MegaReplied = false;
SoftDelay ReplyTimeout;

uint8_t buffer[63];
uint8_t sendBuffer[63];

bool status;

typedef void (*SyncMsgReceived)(const SyncMsg_SyncMsgType);

SyncMsgReceived syncMsgReceivedCb = NULL;

WemosMegaMsgs sendMsg = WemosMegaMsgs_init_zero;

// The loop function is called in an endless loop
void loop()
{
	size_t message_length = 0;

	if (SoftWareSerial.available()) {
		message_length = SoftWareSerial.read();
		debugPrint("length= " +  String(message_length));
		if(message_length < 63) {
			for(uint8_t i = 0; i < message_length; i++) {
				buffer[i] = SoftWareSerial.read();
			}
			processSerial(message_length);

			memset(&buffer[0], 0, sizeof(message_length));
		}
	}

	ArduinoOTA.handle();
}

int lastNetWork = 0;
int lastSignal = -1;

void processSerial(size_t length) {
	MegaWemosMsgs message = MegaWemosMsgs_init_zero;

	/* Create a stream that reads from the buffer. */
	pb_istream_t stream = pb_istream_from_buffer(buffer, length);

	/* Now we are ready to decode the message. */
	status = pb_decode(&stream, MegaWemosMsgs_fields, &message);

	/* Check for errors... */
	if (!status)
	{
		debugPrint("Decoding failed: " + String(PB_GET_ERROR(&stream)));
	}
	else {
		switch(message.which_payload) {
			case MegaWemosMsgs_requestMsg_tag:
				processRequestMsg(message.payload.requestMsg);
				break;
			case MegaWemosMsgs_syncMsg_tag:
				processSyncMsg(message.payload.syncMsg);
				syncMsgReceivedCb(message.payload.syncMsg.type);
				break;
			case MegaWemosMsgs_sensorMsg_tag:
				processSensorMsg(message.payload.sensorMsg);
				break;
		}
	}
}

void processRequestMsg(RequestMsg requestMsg) {
	sendMsg = {0};

	switch(requestMsg.type) {
		case RequestMsg_RequestType_WifiConfigData:
			debugPrint("message: RequestMsg_RequestType_WifiConfigData");
			break;
		case RequestMsg_RequestType_WifiStatus:
			debugPrint("message: RequestMsg_RequestType_WifiStatus");

			sendMsg.which_payload = WemosMegaMsgs_wifiStatusMsg_tag;
			sendMsg.payload.wifiStatusMsg.signal = 100;
			sendMsg.payload.wifiStatusMsg.status = NetworkStatus_CONNECTED;

			syncMsgReceivedCb = [](const SyncMsg_SyncMsgType syncMsg_SyncMsgType) {
				switch(syncMsg_SyncMsgType) {
					case SyncMsg_SyncMsgType::SyncMsg_SyncMsgType_ACK:
						break;
					case SyncMsg_SyncMsgType::SyncMsg_SyncMsgType_ERROR:
						sendMessage(sendMsg);
						break;
					default:
						break;
				}
			};

			sendMessage(sendMsg);

			break;
		case RequestMsg_RequestType_WifiIPAddress:
			debugPrint("message: RequestMsg_RequestType_WifiIPAddress");
			break;
		case RequestMsg_RequestType_WifiNetworks:
			debugPrint("message: RequestMsg_RequestType_WifiNetworks");
			lastNetWork = 0;

			sendMsg.which_payload = WemosMegaMsgs_wifiNetworkMsg_tag;
			String("TEST " + String(lastNetWork)).toCharArray(sendMsg.payload.wifiNetworkMsg.ssid, 20, 0);
			sendMsg.payload.wifiNetworkMsg.signal = 100;
			sendMsg.payload.wifiNetworkMsg.status = NetworkStatus_CONNECTED;

			sendMessage(sendMsg);

			lastNetWork++;
			syncMsgReceivedCb = [](const SyncMsg_SyncMsgType syncMsg_SyncMsgType) {
				switch(syncMsg_SyncMsgType) {
					case SyncMsg_SyncMsgType::SyncMsg_SyncMsgType_ACK:
						if(lastNetWork < 15) {
							sendMsg.which_payload = WemosMegaMsgs_wifiNetworkMsg_tag;
							String("TEST WEMOS1 NETWORK_RX LEVEL " + String(lastNetWork)).toCharArray(sendMsg.payload.wifiNetworkMsg.ssid, 32, 0);
							sendMsg.payload.wifiNetworkMsg.signal = random(101);
							sendMsg.payload.wifiNetworkMsg.status = (NetworkStatus)random(4);
							sendMessage(sendMsg);

							lastNetWork++;
						}
						else {
							lastNetWork = 0;

							sendMsg.payload = {0};
							sendMsg.which_payload = WemosMegaMsgs_syncMsg_tag;
							sendMsg.payload.syncMsg.type = SyncMsg_SyncMsgType_FINISH;
							sendMessage(sendMsg);
						}
						break;
					case SyncMsg_SyncMsgType::SyncMsg_SyncMsgType_ERROR:
						sendMessage(sendMsg);
						break;
					default:
						break;
				}
			};
			break;
	}
}

void processSyncMsg(SyncMsg syncMsg) {
	switch(syncMsg.type) {
		case SyncMsg_SyncMsgType_ACK:
			debugPrint("message: SyncMsg_SyncMsgType_ACK");
			break;
		case SyncMsg_SyncMsgType_ERROR:
			debugPrint("message: SyncMsg_SyncMsgType_ERROR");
			break;
		case SyncMsg_SyncMsgType_FINISH:
			debugPrint("message: SyncMsg_SyncMsgType_FINISH");
			break;
	}
}

void processSensorMsg(SensorMsg sensorMsg) {
	debugPrint("message: SensorMsg");
}

void sendMessage(WemosMegaMsgs message) {
	pb_ostream_t stream = pb_ostream_from_buffer(sendBuffer, sizeof(sendBuffer));
	status = pb_encode(&stream, WemosMegaMsgs_fields, &message);
	size_t message_length = stream.bytes_written;
	if(message_length > 63) {
		debugPrint("Message length is above 63 bytes, not sent!");
		return;
	}
	if (!status)
	{
		debugPrint("Encoding failed: " +  String(PB_GET_ERROR(&stream)));
		return;
	}
	debugPrint("sendMessage length = " + String(message_length));
	SoftWareSerial.write(message_length);
	SoftWareSerial.write(sendBuffer, message_length);
	memset(&sendBuffer[0], 0, sizeof(sendBuffer));
}
/*void processSerial() {

	if(inputString.startsWith("getWifiNetworks")) {
		itemsSent = 0;
		finished = false;
		lastNetWork = random(10);
		lastSignal = random(1, 100);//25 + 27
		String message = "wifinetwork1000";
		int signal = String(message[11, 4]).toInt();
		sendRequest(String(signal));
		sendRequest("{\"wifiNetwork\":{\"ssid\":\"TEST NET " + String(lastNetWork) +
				"\",\"signal\":" + lastSignal + ",\"status\":0,\"pass\":\"***\"}}");
	}
	else if(inputString.startsWith("ack")) {
		delay(50);
		MegaReplied = true;
		itemsSent++;
		if(itemsSent <= itemsCount) {
			lastNetWork = random(10);
			lastSignal = random(1, 100);
			sendRequest("{\"wifiNetwork\":{\"ssid\":\"TEST NET " + String(lastNetWork) +
					"\",\"signal\":" + lastSignal + ",\"status\":0,\"pass\":\"***\"}}");
		}
		else {
			sendRequest("finished");
		}

	}
	else if(inputString.startsWith("resend")) {
		MegaReplied = true;
		if(finished) {
			sendRequest("finished");
		}
		else {
			sendRequest("{\"wifiNetwork\":{\"ssid\":\"TEST NET " + String(lastNetWork) +
					"\",\"signal\":" + lastSignal + ",\"status\":0,\"pass\":\"***\"}}");
		}
	}
	else if(inputString.startsWith("error")) {
		MegaReplied = true;
		sendRequest("{\"wifiNetwork\":{\"ssid\":\"TEST NET " + String(lastNetWork) +
				"\",\"signal\":" + lastSignal + ",\"status\":0,\"pass\":\"***\"}}");
	}
	else if(inputString.startsWith("finished")) {
		MegaReplied = true;
		finished = true;
	}
}*/

void sendRequest(String request) {
	debugPrint(request);
	SoftWareSerial.println(request);
	SoftWareSerial.flush();
	MegaReplied = false;
	Request = request;
	ReplyTimeout.begin(3000);
}

void debugPrint(String data) {
	if(udpDebug) {
		Udp.beginPacket(IpAddress.c_str(), SendUdpPort); //NTP requests are to port 123
		Udp.write((const uint8_t*)data.c_str(), data.length());
		Udp.write('\n');
		Udp.endPacket();
	}
	else {
		//Serial.print(data);
	}
}

void udpBegin(int listenUdpPort) {
	ListenUdpPort = listenUdpPort;

	Udp.begin(ListenUdpPort);
	//Serial.println("Start listening for Udp Debug IPAddress on port: " + String(ListenUdpPort));

	uint32_t beginWait = millis();
	while (millis() - beginWait < udpRetryingTimeMillis) {
		if(hasUdpServerSendIP()) {
			udpDebug = true;
			Udp.stop();
			//Serial.println("Hello World!");
			return;
		}
		delay(50);
		//Serial.print(".");
	}

	Udp.stop();
	//Serial.println();
	//Serial.println("No Udp Debug IPAddress!!! Debugging to Serial...");
}

bool hasUdpServerSendIP() {

	//while (Udp.parsePacket() > 0); // discard any previously received packets

	if(Udp.parsePacket() > 0) {
		Udp.read(_PacketBuffer, _NtpPacketSize);
		String text = String(_PacketBuffer);
		//Serial.println();
		//Serial.println(text);
		if(text.startsWith("debugIP")) {
			//Serial.println("startsWith debugIP");
			IpAddress = text.substring(8);
			//Serial.println("IPAddress set: " + IpAddress);
			if(SendUdpPort > 0) {
				//Serial.println("IPAddress set!");
				return true;
			}
		}
		if(text.startsWith("debugPort")) {
			//Serial.println("startsWith debugPort");
			SendUdpPort = text.substring(10).toInt();
			//Serial.println("SendUdpPort set: " + SendUdpPort);
			if(IpAddress != "") {
				//Serial.println("SendUdpPort set!");
				return true;
			}
		}
	}
	return false;
}
